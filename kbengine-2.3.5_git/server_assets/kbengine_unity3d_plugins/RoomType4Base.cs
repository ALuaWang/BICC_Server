/*
	Generated by KBEngine!
	Please do not modify this file!
	Please inherit this module, such as: (class RoomType4 : RoomType4Base)
	tools = kbcmd
*/

namespace KBEngine
{
	using UnityEngine;
	using System;
	using System.Collections;
	using System.Collections.Generic;

	// defined in */scripts/entity_defs/RoomType4.def
	// Please inherit and implement "class RoomType4 : RoomType4Base"
	public abstract class RoomType4Base : Entity
	{
		public EntityBaseEntityCall_RoomType4Base baseEntityCall = null;
		public EntityCellEntityCall_RoomType4Base cellEntityCall = null;


		public abstract void cellToClient(string arg1, string arg2); 
		public abstract void dealCardsToPlayer(SByte arg1, List<Byte> arg2); 
		public abstract void retChapterSysPrompt(string arg1); 
		public abstract void retLocationIndexs(string arg1); 
		public abstract void retRoomBaseInfo(string arg1); 
		public abstract void updateMapping(string arg1, List<List<Byte>> arg2); 

		public RoomType4Base()
		{
		}

		public override void onGetBase()
		{
			baseEntityCall = new EntityBaseEntityCall_RoomType4Base(id, className);
		}

		public override void onGetCell()
		{
			cellEntityCall = new EntityCellEntityCall_RoomType4Base(id, className);
		}

		public override void onLoseCell()
		{
			cellEntityCall = null;
		}

		public override EntityCall getBaseEntityCall()
		{
			return baseEntityCall;
		}

		public override EntityCall getCellEntityCall()
		{
			return cellEntityCall;
		}

		public override void attachComponents()
		{
		}

		public override void detachComponents()
		{
		}

		public override void onRemoteMethodCall(MemoryStream stream)
		{
			ScriptModule sm = EntityDef.moduledefs["RoomType4"];

			UInt16 methodUtype = 0;
			UInt16 componentPropertyUType = 0;

			if(sm.useMethodDescrAlias)
			{
				componentPropertyUType = stream.readUint8();
				methodUtype = stream.readUint8();
			}
			else
			{
				componentPropertyUType = stream.readUint16();
				methodUtype = stream.readUint16();
			}

			Method method = null;

			if(componentPropertyUType == 0)
			{
				method = sm.idmethods[methodUtype];
			}
			else
			{
				Property pComponentPropertyDescription = sm.idpropertys[componentPropertyUType];
				switch(pComponentPropertyDescription.properUtype)
				{
					default:
						break;
				}

				return;
			}

			switch(method.methodUtype)
			{
				case 64:
					string cellToClient_arg1 = stream.readString();
					string cellToClient_arg2 = stream.readString();
					cellToClient(cellToClient_arg1, cellToClient_arg2);
					break;
				case 61:
					SByte dealCardsToPlayer_arg1 = stream.readInt8();
					List<Byte> dealCardsToPlayer_arg2 = ((DATATYPE_AnonymousArray_32)method.args[1]).createFromStreamEx(stream);
					dealCardsToPlayer(dealCardsToPlayer_arg1, dealCardsToPlayer_arg2);
					break;
				case 60:
					string retChapterSysPrompt_arg1 = stream.readUnicode();
					retChapterSysPrompt(retChapterSysPrompt_arg1);
					break;
				case 63:
					string retLocationIndexs_arg1 = stream.readString();
					retLocationIndexs(retLocationIndexs_arg1);
					break;
				case 62:
					string retRoomBaseInfo_arg1 = stream.readString();
					retRoomBaseInfo(retRoomBaseInfo_arg1);
					break;
				case 51:
					string updateMapping_arg1 = stream.readString();
					List<List<Byte>> updateMapping_arg2 = ((DATATYPE_AnonymousArray_30)method.args[1]).createFromStreamEx(stream);
					updateMapping(updateMapping_arg1, updateMapping_arg2);
					break;
				default:
					break;
			};
		}

		public override void onUpdatePropertys(MemoryStream stream)
		{
			ScriptModule sm = EntityDef.moduledefs["RoomType4"];
			Dictionary<UInt16, Property> pdatas = sm.idpropertys;

			while(stream.length() > 0)
			{
				UInt16 _t_utype = 0;
				UInt16 _t_child_utype = 0;

				{
					if(sm.usePropertyDescrAlias)
					{
						_t_utype = stream.readUint8();
						_t_child_utype = stream.readUint8();
					}
					else
					{
						_t_utype = stream.readUint16();
						_t_child_utype = stream.readUint16();
					}
				}

				Property prop = null;

				if(_t_utype == 0)
				{
					prop = pdatas[_t_child_utype];
				}
				else
				{
					Property pComponentPropertyDescription = pdatas[_t_utype];
					switch(pComponentPropertyDescription.properUtype)
					{
						default:
							break;
					}

					return;
				}

				switch(prop.properUtype)
				{
					case 40001:
						Vector3 oldval_direction = direction;
						direction = stream.readVector3();

						if(prop.isBase())
						{
							if(inited)
								onDirectionChanged(oldval_direction);
						}
						else
						{
							if(inWorld)
								onDirectionChanged(oldval_direction);
						}

						break;
					case 40000:
						Vector3 oldval_position = position;
						position = stream.readVector3();

						if(prop.isBase())
						{
							if(inited)
								onPositionChanged(oldval_position);
						}
						else
						{
							if(inWorld)
								onPositionChanged(oldval_position);
						}

						break;
					case 40002:
						stream.readUint32();
						break;
					default:
						break;
				};
			}
		}

		public override void callPropertysSetMethods()
		{
			ScriptModule sm = EntityDef.moduledefs["RoomType4"];
			Dictionary<UInt16, Property> pdatas = sm.idpropertys;

			Vector3 oldval_direction = direction;
			Property prop_direction = pdatas[2];
			if(prop_direction.isBase())
			{
				if(inited && !inWorld)
					onDirectionChanged(oldval_direction);
			}
			else
			{
				if(inWorld)
				{
					if(prop_direction.isOwnerOnly() && !isPlayer())
					{
					}
					else
					{
						onDirectionChanged(oldval_direction);
					}
				}
			}

			Vector3 oldval_position = position;
			Property prop_position = pdatas[1];
			if(prop_position.isBase())
			{
				if(inited && !inWorld)
					onPositionChanged(oldval_position);
			}
			else
			{
				if(inWorld)
				{
					if(prop_position.isOwnerOnly() && !isPlayer())
					{
					}
					else
					{
						onPositionChanged(oldval_position);
					}
				}
			}

		}
	}
}