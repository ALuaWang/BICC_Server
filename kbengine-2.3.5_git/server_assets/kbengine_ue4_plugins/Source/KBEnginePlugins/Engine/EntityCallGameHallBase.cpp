#include "EntityCallGameHallBase.h"
#include "Bundle.h"


EntityBaseEntityCall_GameHallBase::EntityBaseEntityCall_GameHallBase(int32 eid, const FString& ename) : EntityCall(eid, ename)
{
	type = ENTITYCALL_TYPE_BASE;
}

EntityBaseEntityCall_GameHallBase::~EntityBaseEntityCall_GameHallBase()
{
}



EntityCellEntityCall_GameHallBase::EntityCellEntityCall_GameHallBase(int32 eid, const FString& ename) : EntityCall(eid, ename)
{
	type = ENTITYCALL_TYPE_CELL;
}

EntityCellEntityCall_GameHallBase::~EntityCellEntityCall_GameHallBase()
{
}

