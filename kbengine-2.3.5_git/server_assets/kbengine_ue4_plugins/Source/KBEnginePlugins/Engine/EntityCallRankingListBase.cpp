#include "EntityCallRankingListBase.h"
#include "Bundle.h"


EntityBaseEntityCall_RankingListBase::EntityBaseEntityCall_RankingListBase(int32 eid, const FString& ename) : EntityCall(eid, ename)
{
	type = ENTITYCALL_TYPE_BASE;
}

EntityBaseEntityCall_RankingListBase::~EntityBaseEntityCall_RankingListBase()
{
}



EntityCellEntityCall_RankingListBase::EntityCellEntityCall_RankingListBase(int32 eid, const FString& ename) : EntityCall(eid, ename)
{
	type = ENTITYCALL_TYPE_CELL;
}

EntityCellEntityCall_RankingListBase::~EntityCellEntityCall_RankingListBase()
{
}

