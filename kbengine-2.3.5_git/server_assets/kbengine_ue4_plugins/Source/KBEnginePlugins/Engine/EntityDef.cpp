#include "EntityDef.h"
#include "DataTypes.h"
#include "CustomDataTypes.h"
#include "ScriptModule.h"
#include "Property.h"
#include "Method.h"
#include "KBVar.h"
#include "Entity.h"

#include "Scripts/Account.h"
#include "Scripts/RoomType1.h"
#include "Scripts/RoomType2.h"
#include "Scripts/RoomType3.h"
#include "Scripts/RoomType4.h"
#include "Scripts/RoomType7.h"

TMap<FString, uint16> EntityDef::datatype2id;
TMap<FString, DATATYPE_BASE*> EntityDef::datatypes;
TMap<uint16, DATATYPE_BASE*> EntityDef::id2datatypes;
TMap<FString, int32> EntityDef::entityclass;
TMap<FString, ScriptModule*> EntityDef::moduledefs;
TMap<uint16, ScriptModule*> EntityDef::idmoduledefs;

bool EntityDef::initialize()
{
	initDataTypes();
	initDefTypes();
	initScriptModules();
	return true;
}

bool EntityDef::reset()
{
	clear();
	return initialize();
}

void EntityDef::clear()
{
	TArray<DATATYPE_BASE*> deleted_datatypes;
	for (auto& item : EntityDef::datatypes)
	{
		int32 idx = deleted_datatypes.Find(item.Value);
		if (idx != INDEX_NONE)
			continue;

		deleted_datatypes.Add(item.Value);
		delete item.Value;
	}

	for (auto& item : EntityDef::moduledefs)
		delete item.Value;

	datatype2id.Empty();
	datatypes.Empty();
	id2datatypes.Empty();
	entityclass.Empty();
	moduledefs.Empty();
	idmoduledefs.Empty();
}

void EntityDef::initDataTypes()
{
	datatypes.Add(TEXT("UINT8"), new DATATYPE_UINT8());
	datatypes.Add(TEXT("UINT16"), new DATATYPE_UINT16());
	datatypes.Add(TEXT("UINT32"), new DATATYPE_UINT32());
	datatypes.Add(TEXT("UINT64"), new DATATYPE_UINT64());

	datatypes.Add(TEXT("INT8"), new DATATYPE_INT8());
	datatypes.Add(TEXT("INT16"), new DATATYPE_INT16());
	datatypes.Add(TEXT("INT32"), new DATATYPE_INT32());
	datatypes.Add(TEXT("INT64"), new DATATYPE_INT64());

	datatypes.Add(TEXT("FLOAT"), new DATATYPE_FLOAT());
	datatypes.Add(TEXT("DOUBLE"), new DATATYPE_DOUBLE());

	datatypes.Add(TEXT("STRING"), new DATATYPE_STRING());
	datatypes.Add(TEXT("VECTOR2"), new DATATYPE_VECTOR2());

	datatypes.Add(TEXT("VECTOR3"), new DATATYPE_VECTOR3());

	datatypes.Add(TEXT("VECTOR4"), new DATATYPE_VECTOR4());
	datatypes.Add(TEXT("PYTHON"), new DATATYPE_PYTHON());

	datatypes.Add(TEXT("UNICODE"), new DATATYPE_UNICODE());
	datatypes.Add(TEXT("ENTITYCALL"), new DATATYPE_ENTITYCALL());

	datatypes.Add(TEXT("BLOB"), new DATATYPE_BLOB());
}

Entity* EntityDef::createEntity(int utype)
{
	Entity* pEntity = NULL;

	switch(utype)
	{
		case 1:
			pEntity = new Account();
			break;
		case 5:
			pEntity = new RoomType1();
			break;
		case 6:
			pEntity = new RoomType2();
			break;
		case 7:
			pEntity = new RoomType3();
			break;
		case 8:
			pEntity = new RoomType4();
			break;
		case 9:
			pEntity = new RoomType7();
			break;
		default:
			SCREEN_ERROR_MSG("EntityDef::createEntity() : entity(%d) not found!", utype);
			break;
	};

	return pEntity;
}

void EntityDef::initScriptModules()
{
	ScriptModule* pAccountModule = new ScriptModule("Account", 1);
	EntityDef::moduledefs.Add(TEXT("Account"), pAccountModule);
	EntityDef::idmoduledefs.Add(1, pAccountModule);

	Property* pAccount_position = new Property();
	pAccount_position->name = TEXT("position");
	pAccount_position->properUtype = 40000;
	pAccount_position->properFlags = 4;
	pAccount_position->aliasID = 1;
	KBVar* pAccount_position_defval = new KBVar(FVector());
	pAccount_position->pDefaultVal = pAccount_position_defval;
	pAccountModule->propertys.Add(TEXT("position"), pAccount_position); 

	pAccountModule->usePropertyDescrAlias = true;
	pAccountModule->idpropertys.Add((uint16)pAccount_position->aliasID, pAccount_position);

	//DEBUG_MSG("EntityDef::initScriptModules: add(Account), property(position / 40000).");

	Property* pAccount_direction = new Property();
	pAccount_direction->name = TEXT("direction");
	pAccount_direction->properUtype = 40001;
	pAccount_direction->properFlags = 4;
	pAccount_direction->aliasID = 2;
	KBVar* pAccount_direction_defval = new KBVar(FVector());
	pAccount_direction->pDefaultVal = pAccount_direction_defval;
	pAccountModule->propertys.Add(TEXT("direction"), pAccount_direction); 

	pAccountModule->usePropertyDescrAlias = true;
	pAccountModule->idpropertys.Add((uint16)pAccount_direction->aliasID, pAccount_direction);

	//DEBUG_MSG("EntityDef::initScriptModules: add(Account), property(direction / 40001).");

	Property* pAccount_spaceID = new Property();
	pAccount_spaceID->name = TEXT("spaceID");
	pAccount_spaceID->properUtype = 40002;
	pAccount_spaceID->properFlags = 16;
	pAccount_spaceID->aliasID = 3;
	KBVar* pAccount_spaceID_defval = new KBVar((uint32)FCString::Atoi64(TEXT("")));
	pAccount_spaceID->pDefaultVal = pAccount_spaceID_defval;
	pAccountModule->propertys.Add(TEXT("spaceID"), pAccount_spaceID); 

	pAccountModule->usePropertyDescrAlias = true;
	pAccountModule->idpropertys.Add((uint16)pAccount_spaceID->aliasID, pAccount_spaceID);

	//DEBUG_MSG("EntityDef::initScriptModules: add(Account), property(spaceID / 40002).");

	TArray<DATATYPE_BASE*> Account_baseToClient_args;
	Account_baseToClient_args.Add(EntityDef::id2datatypes[12]);
	Account_baseToClient_args.Add(EntityDef::id2datatypes[12]);

	Method* pAccount_baseToClient = new Method();
	pAccount_baseToClient->name = TEXT("baseToClient");
	pAccount_baseToClient->methodUtype = 7;
	pAccount_baseToClient->aliasID = 1;
	pAccount_baseToClient->args = Account_baseToClient_args;

	pAccountModule->methods.Add(TEXT("baseToClient"), pAccount_baseToClient); 
	pAccountModule->useMethodDescrAlias = true;
	pAccountModule->idmethods.Add((uint16)pAccount_baseToClient->aliasID, pAccount_baseToClient);

	//DEBUG_MSG("EntityDef::initScriptModules: add(Account), method(baseToClient / 7).");

	TArray<DATATYPE_BASE*> Account_cellToClient_args;
	Account_cellToClient_args.Add(EntityDef::id2datatypes[12]);
	Account_cellToClient_args.Add(EntityDef::id2datatypes[12]);

	Method* pAccount_cellToClient = new Method();
	pAccount_cellToClient->name = TEXT("cellToClient");
	pAccount_cellToClient->methodUtype = 8;
	pAccount_cellToClient->aliasID = 2;
	pAccount_cellToClient->args = Account_cellToClient_args;

	pAccountModule->methods.Add(TEXT("cellToClient"), pAccount_cellToClient); 
	pAccountModule->useMethodDescrAlias = true;
	pAccountModule->idmethods.Add((uint16)pAccount_cellToClient->aliasID, pAccount_cellToClient);

	//DEBUG_MSG("EntityDef::initScriptModules: add(Account), method(cellToClient / 8).");

	TArray<DATATYPE_BASE*> Account_clientToBase_args;
	Account_clientToBase_args.Add(EntityDef::id2datatypes[12]);

	Method* pAccount_clientToBase = new Method();
	pAccount_clientToBase->name = TEXT("clientToBase");
	pAccount_clientToBase->methodUtype = 6;
	pAccount_clientToBase->aliasID = -1;
	pAccount_clientToBase->args = Account_clientToBase_args;

	pAccountModule->methods.Add(TEXT("clientToBase"), pAccount_clientToBase); 
	pAccountModule->base_methods.Add(TEXT("clientToBase"), pAccount_clientToBase);

	pAccountModule->idbase_methods.Add(pAccount_clientToBase->methodUtype, pAccount_clientToBase);

	//DEBUG_MSG("EntityDef::initScriptModules: add(Account), method(clientToBase / 6).");

	TArray<DATATYPE_BASE*> Account_clientToCell_args;
	Account_clientToCell_args.Add(EntityDef::id2datatypes[12]);

	Method* pAccount_clientToCell = new Method();
	pAccount_clientToCell->name = TEXT("clientToCell");
	pAccount_clientToCell->methodUtype = 2;
	pAccount_clientToCell->aliasID = -1;
	pAccount_clientToCell->args = Account_clientToCell_args;

	pAccountModule->methods.Add(TEXT("clientToCell"), pAccount_clientToCell); 
	pAccountModule->cell_methods.Add(TEXT("clientToCell"), pAccount_clientToCell);

	pAccountModule->idcell_methods.Add(pAccount_clientToCell->methodUtype, pAccount_clientToCell);

	//DEBUG_MSG("EntityDef::initScriptModules: add(Account), method(clientToCell / 2).");

	ScriptModule* pRoomType1Module = new ScriptModule("RoomType1", 5);
	EntityDef::moduledefs.Add(TEXT("RoomType1"), pRoomType1Module);
	EntityDef::idmoduledefs.Add(5, pRoomType1Module);

	Property* pRoomType1_position = new Property();
	pRoomType1_position->name = TEXT("position");
	pRoomType1_position->properUtype = 40000;
	pRoomType1_position->properFlags = 4;
	pRoomType1_position->aliasID = 1;
	KBVar* pRoomType1_position_defval = new KBVar(FVector());
	pRoomType1_position->pDefaultVal = pRoomType1_position_defval;
	pRoomType1Module->propertys.Add(TEXT("position"), pRoomType1_position); 

	pRoomType1Module->usePropertyDescrAlias = true;
	pRoomType1Module->idpropertys.Add((uint16)pRoomType1_position->aliasID, pRoomType1_position);

	//DEBUG_MSG("EntityDef::initScriptModules: add(RoomType1), property(position / 40000).");

	Property* pRoomType1_direction = new Property();
	pRoomType1_direction->name = TEXT("direction");
	pRoomType1_direction->properUtype = 40001;
	pRoomType1_direction->properFlags = 4;
	pRoomType1_direction->aliasID = 2;
	KBVar* pRoomType1_direction_defval = new KBVar(FVector());
	pRoomType1_direction->pDefaultVal = pRoomType1_direction_defval;
	pRoomType1Module->propertys.Add(TEXT("direction"), pRoomType1_direction); 

	pRoomType1Module->usePropertyDescrAlias = true;
	pRoomType1Module->idpropertys.Add((uint16)pRoomType1_direction->aliasID, pRoomType1_direction);

	//DEBUG_MSG("EntityDef::initScriptModules: add(RoomType1), property(direction / 40001).");

	Property* pRoomType1_spaceID = new Property();
	pRoomType1_spaceID->name = TEXT("spaceID");
	pRoomType1_spaceID->properUtype = 40002;
	pRoomType1_spaceID->properFlags = 16;
	pRoomType1_spaceID->aliasID = 3;
	KBVar* pRoomType1_spaceID_defval = new KBVar((uint32)FCString::Atoi64(TEXT("")));
	pRoomType1_spaceID->pDefaultVal = pRoomType1_spaceID_defval;
	pRoomType1Module->propertys.Add(TEXT("spaceID"), pRoomType1_spaceID); 

	pRoomType1Module->usePropertyDescrAlias = true;
	pRoomType1Module->idpropertys.Add((uint16)pRoomType1_spaceID->aliasID, pRoomType1_spaceID);

	//DEBUG_MSG("EntityDef::initScriptModules: add(RoomType1), property(spaceID / 40002).");

	TArray<DATATYPE_BASE*> RoomType1_cellToClient_args;
	RoomType1_cellToClient_args.Add(EntityDef::id2datatypes[1]);
	RoomType1_cellToClient_args.Add(EntityDef::id2datatypes[1]);

	Method* pRoomType1_cellToClient = new Method();
	pRoomType1_cellToClient->name = TEXT("cellToClient");
	pRoomType1_cellToClient->methodUtype = 22;
	pRoomType1_cellToClient->aliasID = 1;
	pRoomType1_cellToClient->args = RoomType1_cellToClient_args;

	pRoomType1Module->methods.Add(TEXT("cellToClient"), pRoomType1_cellToClient); 
	pRoomType1Module->useMethodDescrAlias = true;
	pRoomType1Module->idmethods.Add((uint16)pRoomType1_cellToClient->aliasID, pRoomType1_cellToClient);

	//DEBUG_MSG("EntityDef::initScriptModules: add(RoomType1), method(cellToClient / 22).");

	TArray<DATATYPE_BASE*> RoomType1_dealCardsToPlayer_args;
	RoomType1_dealCardsToPlayer_args.Add(EntityDef::id2datatypes[6]);
	RoomType1_dealCardsToPlayer_args.Add(EntityDef::id2datatypes[23]);

	Method* pRoomType1_dealCardsToPlayer = new Method();
	pRoomType1_dealCardsToPlayer->name = TEXT("dealCardsToPlayer");
	pRoomType1_dealCardsToPlayer->methodUtype = 19;
	pRoomType1_dealCardsToPlayer->aliasID = 2;
	pRoomType1_dealCardsToPlayer->args = RoomType1_dealCardsToPlayer_args;

	pRoomType1Module->methods.Add(TEXT("dealCardsToPlayer"), pRoomType1_dealCardsToPlayer); 
	pRoomType1Module->useMethodDescrAlias = true;
	pRoomType1Module->idmethods.Add((uint16)pRoomType1_dealCardsToPlayer->aliasID, pRoomType1_dealCardsToPlayer);

	//DEBUG_MSG("EntityDef::initScriptModules: add(RoomType1), method(dealCardsToPlayer / 19).");

	TArray<DATATYPE_BASE*> RoomType1_retChapterSysPrompt_args;
	RoomType1_retChapterSysPrompt_args.Add(EntityDef::id2datatypes[12]);

	Method* pRoomType1_retChapterSysPrompt = new Method();
	pRoomType1_retChapterSysPrompt->name = TEXT("retChapterSysPrompt");
	pRoomType1_retChapterSysPrompt->methodUtype = 18;
	pRoomType1_retChapterSysPrompt->aliasID = 3;
	pRoomType1_retChapterSysPrompt->args = RoomType1_retChapterSysPrompt_args;

	pRoomType1Module->methods.Add(TEXT("retChapterSysPrompt"), pRoomType1_retChapterSysPrompt); 
	pRoomType1Module->useMethodDescrAlias = true;
	pRoomType1Module->idmethods.Add((uint16)pRoomType1_retChapterSysPrompt->aliasID, pRoomType1_retChapterSysPrompt);

	//DEBUG_MSG("EntityDef::initScriptModules: add(RoomType1), method(retChapterSysPrompt / 18).");

	TArray<DATATYPE_BASE*> RoomType1_retLocationIndexs_args;
	RoomType1_retLocationIndexs_args.Add(EntityDef::id2datatypes[1]);

	Method* pRoomType1_retLocationIndexs = new Method();
	pRoomType1_retLocationIndexs->name = TEXT("retLocationIndexs");
	pRoomType1_retLocationIndexs->methodUtype = 21;
	pRoomType1_retLocationIndexs->aliasID = 4;
	pRoomType1_retLocationIndexs->args = RoomType1_retLocationIndexs_args;

	pRoomType1Module->methods.Add(TEXT("retLocationIndexs"), pRoomType1_retLocationIndexs); 
	pRoomType1Module->useMethodDescrAlias = true;
	pRoomType1Module->idmethods.Add((uint16)pRoomType1_retLocationIndexs->aliasID, pRoomType1_retLocationIndexs);

	//DEBUG_MSG("EntityDef::initScriptModules: add(RoomType1), method(retLocationIndexs / 21).");

	TArray<DATATYPE_BASE*> RoomType1_retRoomBaseInfo_args;
	RoomType1_retRoomBaseInfo_args.Add(EntityDef::id2datatypes[1]);

	Method* pRoomType1_retRoomBaseInfo = new Method();
	pRoomType1_retRoomBaseInfo->name = TEXT("retRoomBaseInfo");
	pRoomType1_retRoomBaseInfo->methodUtype = 20;
	pRoomType1_retRoomBaseInfo->aliasID = 5;
	pRoomType1_retRoomBaseInfo->args = RoomType1_retRoomBaseInfo_args;

	pRoomType1Module->methods.Add(TEXT("retRoomBaseInfo"), pRoomType1_retRoomBaseInfo); 
	pRoomType1Module->useMethodDescrAlias = true;
	pRoomType1Module->idmethods.Add((uint16)pRoomType1_retRoomBaseInfo->aliasID, pRoomType1_retRoomBaseInfo);

	//DEBUG_MSG("EntityDef::initScriptModules: add(RoomType1), method(retRoomBaseInfo / 20).");

	TArray<DATATYPE_BASE*> RoomType1_clientToBase_args;
	RoomType1_clientToBase_args.Add(EntityDef::id2datatypes[12]);

	Method* pRoomType1_clientToBase = new Method();
	pRoomType1_clientToBase->name = TEXT("clientToBase");
	pRoomType1_clientToBase->methodUtype = 17;
	pRoomType1_clientToBase->aliasID = -1;
	pRoomType1_clientToBase->args = RoomType1_clientToBase_args;

	pRoomType1Module->methods.Add(TEXT("clientToBase"), pRoomType1_clientToBase); 
	pRoomType1Module->base_methods.Add(TEXT("clientToBase"), pRoomType1_clientToBase);

	pRoomType1Module->idbase_methods.Add(pRoomType1_clientToBase->methodUtype, pRoomType1_clientToBase);

	//DEBUG_MSG("EntityDef::initScriptModules: add(RoomType1), method(clientToBase / 17).");

	TArray<DATATYPE_BASE*> RoomType1_clientReq_args;
	RoomType1_clientReq_args.Add(EntityDef::id2datatypes[1]);

	Method* pRoomType1_clientReq = new Method();
	pRoomType1_clientReq->name = TEXT("clientReq");
	pRoomType1_clientReq->methodUtype = 15;
	pRoomType1_clientReq->aliasID = -1;
	pRoomType1_clientReq->args = RoomType1_clientReq_args;

	pRoomType1Module->methods.Add(TEXT("clientReq"), pRoomType1_clientReq); 
	pRoomType1Module->cell_methods.Add(TEXT("clientReq"), pRoomType1_clientReq);

	pRoomType1Module->idcell_methods.Add(pRoomType1_clientReq->methodUtype, pRoomType1_clientReq);

	//DEBUG_MSG("EntityDef::initScriptModules: add(RoomType1), method(clientReq / 15).");

	TArray<DATATYPE_BASE*> RoomType1_onEnter_args;

	Method* pRoomType1_onEnter = new Method();
	pRoomType1_onEnter->name = TEXT("onEnter");
	pRoomType1_onEnter->methodUtype = 10;
	pRoomType1_onEnter->aliasID = -1;
	pRoomType1_onEnter->args = RoomType1_onEnter_args;

	pRoomType1Module->methods.Add(TEXT("onEnter"), pRoomType1_onEnter); 
	pRoomType1Module->cell_methods.Add(TEXT("onEnter"), pRoomType1_onEnter);

	pRoomType1Module->idcell_methods.Add(pRoomType1_onEnter->methodUtype, pRoomType1_onEnter);

	//DEBUG_MSG("EntityDef::initScriptModules: add(RoomType1), method(onEnter / 10).");

	TArray<DATATYPE_BASE*> RoomType1_playerOperation_args;
	RoomType1_playerOperation_args.Add(EntityDef::id2datatypes[1]);

	Method* pRoomType1_playerOperation = new Method();
	pRoomType1_playerOperation->name = TEXT("playerOperation");
	pRoomType1_playerOperation->methodUtype = 14;
	pRoomType1_playerOperation->aliasID = -1;
	pRoomType1_playerOperation->args = RoomType1_playerOperation_args;

	pRoomType1Module->methods.Add(TEXT("playerOperation"), pRoomType1_playerOperation); 
	pRoomType1Module->cell_methods.Add(TEXT("playerOperation"), pRoomType1_playerOperation);

	pRoomType1Module->idcell_methods.Add(pRoomType1_playerOperation->methodUtype, pRoomType1_playerOperation);

	//DEBUG_MSG("EntityDef::initScriptModules: add(RoomType1), method(playerOperation / 14).");

	TArray<DATATYPE_BASE*> RoomType1_reqRoomBaseInfo_args;

	Method* pRoomType1_reqRoomBaseInfo = new Method();
	pRoomType1_reqRoomBaseInfo->name = TEXT("reqRoomBaseInfo");
	pRoomType1_reqRoomBaseInfo->methodUtype = 13;
	pRoomType1_reqRoomBaseInfo->aliasID = -1;
	pRoomType1_reqRoomBaseInfo->args = RoomType1_reqRoomBaseInfo_args;

	pRoomType1Module->methods.Add(TEXT("reqRoomBaseInfo"), pRoomType1_reqRoomBaseInfo); 
	pRoomType1Module->cell_methods.Add(TEXT("reqRoomBaseInfo"), pRoomType1_reqRoomBaseInfo);

	pRoomType1Module->idcell_methods.Add(pRoomType1_reqRoomBaseInfo->methodUtype, pRoomType1_reqRoomBaseInfo);

	//DEBUG_MSG("EntityDef::initScriptModules: add(RoomType1), method(reqRoomBaseInfo / 13).");

	ScriptModule* pRoomType2Module = new ScriptModule("RoomType2", 6);
	EntityDef::moduledefs.Add(TEXT("RoomType2"), pRoomType2Module);
	EntityDef::idmoduledefs.Add(6, pRoomType2Module);

	Property* pRoomType2_position = new Property();
	pRoomType2_position->name = TEXT("position");
	pRoomType2_position->properUtype = 40000;
	pRoomType2_position->properFlags = 4;
	pRoomType2_position->aliasID = 1;
	KBVar* pRoomType2_position_defval = new KBVar(FVector());
	pRoomType2_position->pDefaultVal = pRoomType2_position_defval;
	pRoomType2Module->propertys.Add(TEXT("position"), pRoomType2_position); 

	pRoomType2Module->usePropertyDescrAlias = true;
	pRoomType2Module->idpropertys.Add((uint16)pRoomType2_position->aliasID, pRoomType2_position);

	//DEBUG_MSG("EntityDef::initScriptModules: add(RoomType2), property(position / 40000).");

	Property* pRoomType2_direction = new Property();
	pRoomType2_direction->name = TEXT("direction");
	pRoomType2_direction->properUtype = 40001;
	pRoomType2_direction->properFlags = 4;
	pRoomType2_direction->aliasID = 2;
	KBVar* pRoomType2_direction_defval = new KBVar(FVector());
	pRoomType2_direction->pDefaultVal = pRoomType2_direction_defval;
	pRoomType2Module->propertys.Add(TEXT("direction"), pRoomType2_direction); 

	pRoomType2Module->usePropertyDescrAlias = true;
	pRoomType2Module->idpropertys.Add((uint16)pRoomType2_direction->aliasID, pRoomType2_direction);

	//DEBUG_MSG("EntityDef::initScriptModules: add(RoomType2), property(direction / 40001).");

	Property* pRoomType2_spaceID = new Property();
	pRoomType2_spaceID->name = TEXT("spaceID");
	pRoomType2_spaceID->properUtype = 40002;
	pRoomType2_spaceID->properFlags = 16;
	pRoomType2_spaceID->aliasID = 3;
	KBVar* pRoomType2_spaceID_defval = new KBVar((uint32)FCString::Atoi64(TEXT("")));
	pRoomType2_spaceID->pDefaultVal = pRoomType2_spaceID_defval;
	pRoomType2Module->propertys.Add(TEXT("spaceID"), pRoomType2_spaceID); 

	pRoomType2Module->usePropertyDescrAlias = true;
	pRoomType2Module->idpropertys.Add((uint16)pRoomType2_spaceID->aliasID, pRoomType2_spaceID);

	//DEBUG_MSG("EntityDef::initScriptModules: add(RoomType2), property(spaceID / 40002).");

	TArray<DATATYPE_BASE*> RoomType2_cellToClient_args;
	RoomType2_cellToClient_args.Add(EntityDef::id2datatypes[1]);
	RoomType2_cellToClient_args.Add(EntityDef::id2datatypes[1]);

	Method* pRoomType2_cellToClient = new Method();
	pRoomType2_cellToClient->name = TEXT("cellToClient");
	pRoomType2_cellToClient->methodUtype = 36;
	pRoomType2_cellToClient->aliasID = 1;
	pRoomType2_cellToClient->args = RoomType2_cellToClient_args;

	pRoomType2Module->methods.Add(TEXT("cellToClient"), pRoomType2_cellToClient); 
	pRoomType2Module->useMethodDescrAlias = true;
	pRoomType2Module->idmethods.Add((uint16)pRoomType2_cellToClient->aliasID, pRoomType2_cellToClient);

	//DEBUG_MSG("EntityDef::initScriptModules: add(RoomType2), method(cellToClient / 36).");

	TArray<DATATYPE_BASE*> RoomType2_dealCardsToPlayer_args;
	RoomType2_dealCardsToPlayer_args.Add(EntityDef::id2datatypes[6]);
	RoomType2_dealCardsToPlayer_args.Add(EntityDef::id2datatypes[26]);

	Method* pRoomType2_dealCardsToPlayer = new Method();
	pRoomType2_dealCardsToPlayer->name = TEXT("dealCardsToPlayer");
	pRoomType2_dealCardsToPlayer->methodUtype = 33;
	pRoomType2_dealCardsToPlayer->aliasID = 2;
	pRoomType2_dealCardsToPlayer->args = RoomType2_dealCardsToPlayer_args;

	pRoomType2Module->methods.Add(TEXT("dealCardsToPlayer"), pRoomType2_dealCardsToPlayer); 
	pRoomType2Module->useMethodDescrAlias = true;
	pRoomType2Module->idmethods.Add((uint16)pRoomType2_dealCardsToPlayer->aliasID, pRoomType2_dealCardsToPlayer);

	//DEBUG_MSG("EntityDef::initScriptModules: add(RoomType2), method(dealCardsToPlayer / 33).");

	TArray<DATATYPE_BASE*> RoomType2_retChapterSysPrompt_args;
	RoomType2_retChapterSysPrompt_args.Add(EntityDef::id2datatypes[12]);

	Method* pRoomType2_retChapterSysPrompt = new Method();
	pRoomType2_retChapterSysPrompt->name = TEXT("retChapterSysPrompt");
	pRoomType2_retChapterSysPrompt->methodUtype = 32;
	pRoomType2_retChapterSysPrompt->aliasID = 3;
	pRoomType2_retChapterSysPrompt->args = RoomType2_retChapterSysPrompt_args;

	pRoomType2Module->methods.Add(TEXT("retChapterSysPrompt"), pRoomType2_retChapterSysPrompt); 
	pRoomType2Module->useMethodDescrAlias = true;
	pRoomType2Module->idmethods.Add((uint16)pRoomType2_retChapterSysPrompt->aliasID, pRoomType2_retChapterSysPrompt);

	//DEBUG_MSG("EntityDef::initScriptModules: add(RoomType2), method(retChapterSysPrompt / 32).");

	TArray<DATATYPE_BASE*> RoomType2_retLocationIndexs_args;
	RoomType2_retLocationIndexs_args.Add(EntityDef::id2datatypes[1]);

	Method* pRoomType2_retLocationIndexs = new Method();
	pRoomType2_retLocationIndexs->name = TEXT("retLocationIndexs");
	pRoomType2_retLocationIndexs->methodUtype = 35;
	pRoomType2_retLocationIndexs->aliasID = 4;
	pRoomType2_retLocationIndexs->args = RoomType2_retLocationIndexs_args;

	pRoomType2Module->methods.Add(TEXT("retLocationIndexs"), pRoomType2_retLocationIndexs); 
	pRoomType2Module->useMethodDescrAlias = true;
	pRoomType2Module->idmethods.Add((uint16)pRoomType2_retLocationIndexs->aliasID, pRoomType2_retLocationIndexs);

	//DEBUG_MSG("EntityDef::initScriptModules: add(RoomType2), method(retLocationIndexs / 35).");

	TArray<DATATYPE_BASE*> RoomType2_retRoomBaseInfo_args;
	RoomType2_retRoomBaseInfo_args.Add(EntityDef::id2datatypes[1]);

	Method* pRoomType2_retRoomBaseInfo = new Method();
	pRoomType2_retRoomBaseInfo->name = TEXT("retRoomBaseInfo");
	pRoomType2_retRoomBaseInfo->methodUtype = 34;
	pRoomType2_retRoomBaseInfo->aliasID = 5;
	pRoomType2_retRoomBaseInfo->args = RoomType2_retRoomBaseInfo_args;

	pRoomType2Module->methods.Add(TEXT("retRoomBaseInfo"), pRoomType2_retRoomBaseInfo); 
	pRoomType2Module->useMethodDescrAlias = true;
	pRoomType2Module->idmethods.Add((uint16)pRoomType2_retRoomBaseInfo->aliasID, pRoomType2_retRoomBaseInfo);

	//DEBUG_MSG("EntityDef::initScriptModules: add(RoomType2), method(retRoomBaseInfo / 34).");

	TArray<DATATYPE_BASE*> RoomType2_updateMapping_args;
	RoomType2_updateMapping_args.Add(EntityDef::id2datatypes[1]);
	RoomType2_updateMapping_args.Add(EntityDef::id2datatypes[24]);

	Method* pRoomType2_updateMapping = new Method();
	pRoomType2_updateMapping->name = TEXT("updateMapping");
	pRoomType2_updateMapping->methodUtype = 23;
	pRoomType2_updateMapping->aliasID = 6;
	pRoomType2_updateMapping->args = RoomType2_updateMapping_args;

	pRoomType2Module->methods.Add(TEXT("updateMapping"), pRoomType2_updateMapping); 
	pRoomType2Module->useMethodDescrAlias = true;
	pRoomType2Module->idmethods.Add((uint16)pRoomType2_updateMapping->aliasID, pRoomType2_updateMapping);

	//DEBUG_MSG("EntityDef::initScriptModules: add(RoomType2), method(updateMapping / 23).");

	TArray<DATATYPE_BASE*> RoomType2_clientToBase_args;
	RoomType2_clientToBase_args.Add(EntityDef::id2datatypes[12]);

	Method* pRoomType2_clientToBase = new Method();
	pRoomType2_clientToBase->name = TEXT("clientToBase");
	pRoomType2_clientToBase->methodUtype = 31;
	pRoomType2_clientToBase->aliasID = -1;
	pRoomType2_clientToBase->args = RoomType2_clientToBase_args;

	pRoomType2Module->methods.Add(TEXT("clientToBase"), pRoomType2_clientToBase); 
	pRoomType2Module->base_methods.Add(TEXT("clientToBase"), pRoomType2_clientToBase);

	pRoomType2Module->idbase_methods.Add(pRoomType2_clientToBase->methodUtype, pRoomType2_clientToBase);

	//DEBUG_MSG("EntityDef::initScriptModules: add(RoomType2), method(clientToBase / 31).");

	TArray<DATATYPE_BASE*> RoomType2_clientReq_args;
	RoomType2_clientReq_args.Add(EntityDef::id2datatypes[1]);

	Method* pRoomType2_clientReq = new Method();
	pRoomType2_clientReq->name = TEXT("clientReq");
	pRoomType2_clientReq->methodUtype = 29;
	pRoomType2_clientReq->aliasID = -1;
	pRoomType2_clientReq->args = RoomType2_clientReq_args;

	pRoomType2Module->methods.Add(TEXT("clientReq"), pRoomType2_clientReq); 
	pRoomType2Module->cell_methods.Add(TEXT("clientReq"), pRoomType2_clientReq);

	pRoomType2Module->idcell_methods.Add(pRoomType2_clientReq->methodUtype, pRoomType2_clientReq);

	//DEBUG_MSG("EntityDef::initScriptModules: add(RoomType2), method(clientReq / 29).");

	TArray<DATATYPE_BASE*> RoomType2_onEnter_args;

	Method* pRoomType2_onEnter = new Method();
	pRoomType2_onEnter->name = TEXT("onEnter");
	pRoomType2_onEnter->methodUtype = 24;
	pRoomType2_onEnter->aliasID = -1;
	pRoomType2_onEnter->args = RoomType2_onEnter_args;

	pRoomType2Module->methods.Add(TEXT("onEnter"), pRoomType2_onEnter); 
	pRoomType2Module->cell_methods.Add(TEXT("onEnter"), pRoomType2_onEnter);

	pRoomType2Module->idcell_methods.Add(pRoomType2_onEnter->methodUtype, pRoomType2_onEnter);

	//DEBUG_MSG("EntityDef::initScriptModules: add(RoomType2), method(onEnter / 24).");

	TArray<DATATYPE_BASE*> RoomType2_playerOperation_args;
	RoomType2_playerOperation_args.Add(EntityDef::id2datatypes[1]);

	Method* pRoomType2_playerOperation = new Method();
	pRoomType2_playerOperation->name = TEXT("playerOperation");
	pRoomType2_playerOperation->methodUtype = 28;
	pRoomType2_playerOperation->aliasID = -1;
	pRoomType2_playerOperation->args = RoomType2_playerOperation_args;

	pRoomType2Module->methods.Add(TEXT("playerOperation"), pRoomType2_playerOperation); 
	pRoomType2Module->cell_methods.Add(TEXT("playerOperation"), pRoomType2_playerOperation);

	pRoomType2Module->idcell_methods.Add(pRoomType2_playerOperation->methodUtype, pRoomType2_playerOperation);

	//DEBUG_MSG("EntityDef::initScriptModules: add(RoomType2), method(playerOperation / 28).");

	TArray<DATATYPE_BASE*> RoomType2_reqRoomBaseInfo_args;

	Method* pRoomType2_reqRoomBaseInfo = new Method();
	pRoomType2_reqRoomBaseInfo->name = TEXT("reqRoomBaseInfo");
	pRoomType2_reqRoomBaseInfo->methodUtype = 27;
	pRoomType2_reqRoomBaseInfo->aliasID = -1;
	pRoomType2_reqRoomBaseInfo->args = RoomType2_reqRoomBaseInfo_args;

	pRoomType2Module->methods.Add(TEXT("reqRoomBaseInfo"), pRoomType2_reqRoomBaseInfo); 
	pRoomType2Module->cell_methods.Add(TEXT("reqRoomBaseInfo"), pRoomType2_reqRoomBaseInfo);

	pRoomType2Module->idcell_methods.Add(pRoomType2_reqRoomBaseInfo->methodUtype, pRoomType2_reqRoomBaseInfo);

	//DEBUG_MSG("EntityDef::initScriptModules: add(RoomType2), method(reqRoomBaseInfo / 27).");

	ScriptModule* pRoomType3Module = new ScriptModule("RoomType3", 7);
	EntityDef::moduledefs.Add(TEXT("RoomType3"), pRoomType3Module);
	EntityDef::idmoduledefs.Add(7, pRoomType3Module);

	Property* pRoomType3_position = new Property();
	pRoomType3_position->name = TEXT("position");
	pRoomType3_position->properUtype = 40000;
	pRoomType3_position->properFlags = 4;
	pRoomType3_position->aliasID = 1;
	KBVar* pRoomType3_position_defval = new KBVar(FVector());
	pRoomType3_position->pDefaultVal = pRoomType3_position_defval;
	pRoomType3Module->propertys.Add(TEXT("position"), pRoomType3_position); 

	pRoomType3Module->usePropertyDescrAlias = true;
	pRoomType3Module->idpropertys.Add((uint16)pRoomType3_position->aliasID, pRoomType3_position);

	//DEBUG_MSG("EntityDef::initScriptModules: add(RoomType3), property(position / 40000).");

	Property* pRoomType3_direction = new Property();
	pRoomType3_direction->name = TEXT("direction");
	pRoomType3_direction->properUtype = 40001;
	pRoomType3_direction->properFlags = 4;
	pRoomType3_direction->aliasID = 2;
	KBVar* pRoomType3_direction_defval = new KBVar(FVector());
	pRoomType3_direction->pDefaultVal = pRoomType3_direction_defval;
	pRoomType3Module->propertys.Add(TEXT("direction"), pRoomType3_direction); 

	pRoomType3Module->usePropertyDescrAlias = true;
	pRoomType3Module->idpropertys.Add((uint16)pRoomType3_direction->aliasID, pRoomType3_direction);

	//DEBUG_MSG("EntityDef::initScriptModules: add(RoomType3), property(direction / 40001).");

	Property* pRoomType3_spaceID = new Property();
	pRoomType3_spaceID->name = TEXT("spaceID");
	pRoomType3_spaceID->properUtype = 40002;
	pRoomType3_spaceID->properFlags = 16;
	pRoomType3_spaceID->aliasID = 3;
	KBVar* pRoomType3_spaceID_defval = new KBVar((uint32)FCString::Atoi64(TEXT("")));
	pRoomType3_spaceID->pDefaultVal = pRoomType3_spaceID_defval;
	pRoomType3Module->propertys.Add(TEXT("spaceID"), pRoomType3_spaceID); 

	pRoomType3Module->usePropertyDescrAlias = true;
	pRoomType3Module->idpropertys.Add((uint16)pRoomType3_spaceID->aliasID, pRoomType3_spaceID);

	//DEBUG_MSG("EntityDef::initScriptModules: add(RoomType3), property(spaceID / 40002).");

	TArray<DATATYPE_BASE*> RoomType3_cellToClient_args;
	RoomType3_cellToClient_args.Add(EntityDef::id2datatypes[1]);
	RoomType3_cellToClient_args.Add(EntityDef::id2datatypes[1]);

	Method* pRoomType3_cellToClient = new Method();
	pRoomType3_cellToClient->name = TEXT("cellToClient");
	pRoomType3_cellToClient->methodUtype = 50;
	pRoomType3_cellToClient->aliasID = 1;
	pRoomType3_cellToClient->args = RoomType3_cellToClient_args;

	pRoomType3Module->methods.Add(TEXT("cellToClient"), pRoomType3_cellToClient); 
	pRoomType3Module->useMethodDescrAlias = true;
	pRoomType3Module->idmethods.Add((uint16)pRoomType3_cellToClient->aliasID, pRoomType3_cellToClient);

	//DEBUG_MSG("EntityDef::initScriptModules: add(RoomType3), method(cellToClient / 50).");

	TArray<DATATYPE_BASE*> RoomType3_dealCardsToPlayer_args;
	RoomType3_dealCardsToPlayer_args.Add(EntityDef::id2datatypes[6]);
	RoomType3_dealCardsToPlayer_args.Add(EntityDef::id2datatypes[29]);

	Method* pRoomType3_dealCardsToPlayer = new Method();
	pRoomType3_dealCardsToPlayer->name = TEXT("dealCardsToPlayer");
	pRoomType3_dealCardsToPlayer->methodUtype = 47;
	pRoomType3_dealCardsToPlayer->aliasID = 2;
	pRoomType3_dealCardsToPlayer->args = RoomType3_dealCardsToPlayer_args;

	pRoomType3Module->methods.Add(TEXT("dealCardsToPlayer"), pRoomType3_dealCardsToPlayer); 
	pRoomType3Module->useMethodDescrAlias = true;
	pRoomType3Module->idmethods.Add((uint16)pRoomType3_dealCardsToPlayer->aliasID, pRoomType3_dealCardsToPlayer);

	//DEBUG_MSG("EntityDef::initScriptModules: add(RoomType3), method(dealCardsToPlayer / 47).");

	TArray<DATATYPE_BASE*> RoomType3_retChapterSysPrompt_args;
	RoomType3_retChapterSysPrompt_args.Add(EntityDef::id2datatypes[12]);

	Method* pRoomType3_retChapterSysPrompt = new Method();
	pRoomType3_retChapterSysPrompt->name = TEXT("retChapterSysPrompt");
	pRoomType3_retChapterSysPrompt->methodUtype = 46;
	pRoomType3_retChapterSysPrompt->aliasID = 3;
	pRoomType3_retChapterSysPrompt->args = RoomType3_retChapterSysPrompt_args;

	pRoomType3Module->methods.Add(TEXT("retChapterSysPrompt"), pRoomType3_retChapterSysPrompt); 
	pRoomType3Module->useMethodDescrAlias = true;
	pRoomType3Module->idmethods.Add((uint16)pRoomType3_retChapterSysPrompt->aliasID, pRoomType3_retChapterSysPrompt);

	//DEBUG_MSG("EntityDef::initScriptModules: add(RoomType3), method(retChapterSysPrompt / 46).");

	TArray<DATATYPE_BASE*> RoomType3_retLocationIndexs_args;
	RoomType3_retLocationIndexs_args.Add(EntityDef::id2datatypes[1]);

	Method* pRoomType3_retLocationIndexs = new Method();
	pRoomType3_retLocationIndexs->name = TEXT("retLocationIndexs");
	pRoomType3_retLocationIndexs->methodUtype = 49;
	pRoomType3_retLocationIndexs->aliasID = 4;
	pRoomType3_retLocationIndexs->args = RoomType3_retLocationIndexs_args;

	pRoomType3Module->methods.Add(TEXT("retLocationIndexs"), pRoomType3_retLocationIndexs); 
	pRoomType3Module->useMethodDescrAlias = true;
	pRoomType3Module->idmethods.Add((uint16)pRoomType3_retLocationIndexs->aliasID, pRoomType3_retLocationIndexs);

	//DEBUG_MSG("EntityDef::initScriptModules: add(RoomType3), method(retLocationIndexs / 49).");

	TArray<DATATYPE_BASE*> RoomType3_retRoomBaseInfo_args;
	RoomType3_retRoomBaseInfo_args.Add(EntityDef::id2datatypes[1]);

	Method* pRoomType3_retRoomBaseInfo = new Method();
	pRoomType3_retRoomBaseInfo->name = TEXT("retRoomBaseInfo");
	pRoomType3_retRoomBaseInfo->methodUtype = 48;
	pRoomType3_retRoomBaseInfo->aliasID = 5;
	pRoomType3_retRoomBaseInfo->args = RoomType3_retRoomBaseInfo_args;

	pRoomType3Module->methods.Add(TEXT("retRoomBaseInfo"), pRoomType3_retRoomBaseInfo); 
	pRoomType3Module->useMethodDescrAlias = true;
	pRoomType3Module->idmethods.Add((uint16)pRoomType3_retRoomBaseInfo->aliasID, pRoomType3_retRoomBaseInfo);

	//DEBUG_MSG("EntityDef::initScriptModules: add(RoomType3), method(retRoomBaseInfo / 48).");

	TArray<DATATYPE_BASE*> RoomType3_updateMapping_args;
	RoomType3_updateMapping_args.Add(EntityDef::id2datatypes[1]);
	RoomType3_updateMapping_args.Add(EntityDef::id2datatypes[27]);

	Method* pRoomType3_updateMapping = new Method();
	pRoomType3_updateMapping->name = TEXT("updateMapping");
	pRoomType3_updateMapping->methodUtype = 37;
	pRoomType3_updateMapping->aliasID = 6;
	pRoomType3_updateMapping->args = RoomType3_updateMapping_args;

	pRoomType3Module->methods.Add(TEXT("updateMapping"), pRoomType3_updateMapping); 
	pRoomType3Module->useMethodDescrAlias = true;
	pRoomType3Module->idmethods.Add((uint16)pRoomType3_updateMapping->aliasID, pRoomType3_updateMapping);

	//DEBUG_MSG("EntityDef::initScriptModules: add(RoomType3), method(updateMapping / 37).");

	TArray<DATATYPE_BASE*> RoomType3_clientToBase_args;
	RoomType3_clientToBase_args.Add(EntityDef::id2datatypes[12]);

	Method* pRoomType3_clientToBase = new Method();
	pRoomType3_clientToBase->name = TEXT("clientToBase");
	pRoomType3_clientToBase->methodUtype = 45;
	pRoomType3_clientToBase->aliasID = -1;
	pRoomType3_clientToBase->args = RoomType3_clientToBase_args;

	pRoomType3Module->methods.Add(TEXT("clientToBase"), pRoomType3_clientToBase); 
	pRoomType3Module->base_methods.Add(TEXT("clientToBase"), pRoomType3_clientToBase);

	pRoomType3Module->idbase_methods.Add(pRoomType3_clientToBase->methodUtype, pRoomType3_clientToBase);

	//DEBUG_MSG("EntityDef::initScriptModules: add(RoomType3), method(clientToBase / 45).");

	TArray<DATATYPE_BASE*> RoomType3_clientReq_args;
	RoomType3_clientReq_args.Add(EntityDef::id2datatypes[1]);

	Method* pRoomType3_clientReq = new Method();
	pRoomType3_clientReq->name = TEXT("clientReq");
	pRoomType3_clientReq->methodUtype = 43;
	pRoomType3_clientReq->aliasID = -1;
	pRoomType3_clientReq->args = RoomType3_clientReq_args;

	pRoomType3Module->methods.Add(TEXT("clientReq"), pRoomType3_clientReq); 
	pRoomType3Module->cell_methods.Add(TEXT("clientReq"), pRoomType3_clientReq);

	pRoomType3Module->idcell_methods.Add(pRoomType3_clientReq->methodUtype, pRoomType3_clientReq);

	//DEBUG_MSG("EntityDef::initScriptModules: add(RoomType3), method(clientReq / 43).");

	TArray<DATATYPE_BASE*> RoomType3_onEnter_args;

	Method* pRoomType3_onEnter = new Method();
	pRoomType3_onEnter->name = TEXT("onEnter");
	pRoomType3_onEnter->methodUtype = 38;
	pRoomType3_onEnter->aliasID = -1;
	pRoomType3_onEnter->args = RoomType3_onEnter_args;

	pRoomType3Module->methods.Add(TEXT("onEnter"), pRoomType3_onEnter); 
	pRoomType3Module->cell_methods.Add(TEXT("onEnter"), pRoomType3_onEnter);

	pRoomType3Module->idcell_methods.Add(pRoomType3_onEnter->methodUtype, pRoomType3_onEnter);

	//DEBUG_MSG("EntityDef::initScriptModules: add(RoomType3), method(onEnter / 38).");

	TArray<DATATYPE_BASE*> RoomType3_playerOperation_args;
	RoomType3_playerOperation_args.Add(EntityDef::id2datatypes[1]);

	Method* pRoomType3_playerOperation = new Method();
	pRoomType3_playerOperation->name = TEXT("playerOperation");
	pRoomType3_playerOperation->methodUtype = 42;
	pRoomType3_playerOperation->aliasID = -1;
	pRoomType3_playerOperation->args = RoomType3_playerOperation_args;

	pRoomType3Module->methods.Add(TEXT("playerOperation"), pRoomType3_playerOperation); 
	pRoomType3Module->cell_methods.Add(TEXT("playerOperation"), pRoomType3_playerOperation);

	pRoomType3Module->idcell_methods.Add(pRoomType3_playerOperation->methodUtype, pRoomType3_playerOperation);

	//DEBUG_MSG("EntityDef::initScriptModules: add(RoomType3), method(playerOperation / 42).");

	TArray<DATATYPE_BASE*> RoomType3_reqRoomBaseInfo_args;

	Method* pRoomType3_reqRoomBaseInfo = new Method();
	pRoomType3_reqRoomBaseInfo->name = TEXT("reqRoomBaseInfo");
	pRoomType3_reqRoomBaseInfo->methodUtype = 41;
	pRoomType3_reqRoomBaseInfo->aliasID = -1;
	pRoomType3_reqRoomBaseInfo->args = RoomType3_reqRoomBaseInfo_args;

	pRoomType3Module->methods.Add(TEXT("reqRoomBaseInfo"), pRoomType3_reqRoomBaseInfo); 
	pRoomType3Module->cell_methods.Add(TEXT("reqRoomBaseInfo"), pRoomType3_reqRoomBaseInfo);

	pRoomType3Module->idcell_methods.Add(pRoomType3_reqRoomBaseInfo->methodUtype, pRoomType3_reqRoomBaseInfo);

	//DEBUG_MSG("EntityDef::initScriptModules: add(RoomType3), method(reqRoomBaseInfo / 41).");

	ScriptModule* pRoomType4Module = new ScriptModule("RoomType4", 8);
	EntityDef::moduledefs.Add(TEXT("RoomType4"), pRoomType4Module);
	EntityDef::idmoduledefs.Add(8, pRoomType4Module);

	Property* pRoomType4_position = new Property();
	pRoomType4_position->name = TEXT("position");
	pRoomType4_position->properUtype = 40000;
	pRoomType4_position->properFlags = 4;
	pRoomType4_position->aliasID = 1;
	KBVar* pRoomType4_position_defval = new KBVar(FVector());
	pRoomType4_position->pDefaultVal = pRoomType4_position_defval;
	pRoomType4Module->propertys.Add(TEXT("position"), pRoomType4_position); 

	pRoomType4Module->usePropertyDescrAlias = true;
	pRoomType4Module->idpropertys.Add((uint16)pRoomType4_position->aliasID, pRoomType4_position);

	//DEBUG_MSG("EntityDef::initScriptModules: add(RoomType4), property(position / 40000).");

	Property* pRoomType4_direction = new Property();
	pRoomType4_direction->name = TEXT("direction");
	pRoomType4_direction->properUtype = 40001;
	pRoomType4_direction->properFlags = 4;
	pRoomType4_direction->aliasID = 2;
	KBVar* pRoomType4_direction_defval = new KBVar(FVector());
	pRoomType4_direction->pDefaultVal = pRoomType4_direction_defval;
	pRoomType4Module->propertys.Add(TEXT("direction"), pRoomType4_direction); 

	pRoomType4Module->usePropertyDescrAlias = true;
	pRoomType4Module->idpropertys.Add((uint16)pRoomType4_direction->aliasID, pRoomType4_direction);

	//DEBUG_MSG("EntityDef::initScriptModules: add(RoomType4), property(direction / 40001).");

	Property* pRoomType4_spaceID = new Property();
	pRoomType4_spaceID->name = TEXT("spaceID");
	pRoomType4_spaceID->properUtype = 40002;
	pRoomType4_spaceID->properFlags = 16;
	pRoomType4_spaceID->aliasID = 3;
	KBVar* pRoomType4_spaceID_defval = new KBVar((uint32)FCString::Atoi64(TEXT("")));
	pRoomType4_spaceID->pDefaultVal = pRoomType4_spaceID_defval;
	pRoomType4Module->propertys.Add(TEXT("spaceID"), pRoomType4_spaceID); 

	pRoomType4Module->usePropertyDescrAlias = true;
	pRoomType4Module->idpropertys.Add((uint16)pRoomType4_spaceID->aliasID, pRoomType4_spaceID);

	//DEBUG_MSG("EntityDef::initScriptModules: add(RoomType4), property(spaceID / 40002).");

	TArray<DATATYPE_BASE*> RoomType4_cellToClient_args;
	RoomType4_cellToClient_args.Add(EntityDef::id2datatypes[1]);
	RoomType4_cellToClient_args.Add(EntityDef::id2datatypes[1]);

	Method* pRoomType4_cellToClient = new Method();
	pRoomType4_cellToClient->name = TEXT("cellToClient");
	pRoomType4_cellToClient->methodUtype = 64;
	pRoomType4_cellToClient->aliasID = 1;
	pRoomType4_cellToClient->args = RoomType4_cellToClient_args;

	pRoomType4Module->methods.Add(TEXT("cellToClient"), pRoomType4_cellToClient); 
	pRoomType4Module->useMethodDescrAlias = true;
	pRoomType4Module->idmethods.Add((uint16)pRoomType4_cellToClient->aliasID, pRoomType4_cellToClient);

	//DEBUG_MSG("EntityDef::initScriptModules: add(RoomType4), method(cellToClient / 64).");

	TArray<DATATYPE_BASE*> RoomType4_dealCardsToPlayer_args;
	RoomType4_dealCardsToPlayer_args.Add(EntityDef::id2datatypes[6]);
	RoomType4_dealCardsToPlayer_args.Add(EntityDef::id2datatypes[32]);

	Method* pRoomType4_dealCardsToPlayer = new Method();
	pRoomType4_dealCardsToPlayer->name = TEXT("dealCardsToPlayer");
	pRoomType4_dealCardsToPlayer->methodUtype = 61;
	pRoomType4_dealCardsToPlayer->aliasID = 2;
	pRoomType4_dealCardsToPlayer->args = RoomType4_dealCardsToPlayer_args;

	pRoomType4Module->methods.Add(TEXT("dealCardsToPlayer"), pRoomType4_dealCardsToPlayer); 
	pRoomType4Module->useMethodDescrAlias = true;
	pRoomType4Module->idmethods.Add((uint16)pRoomType4_dealCardsToPlayer->aliasID, pRoomType4_dealCardsToPlayer);

	//DEBUG_MSG("EntityDef::initScriptModules: add(RoomType4), method(dealCardsToPlayer / 61).");

	TArray<DATATYPE_BASE*> RoomType4_retChapterSysPrompt_args;
	RoomType4_retChapterSysPrompt_args.Add(EntityDef::id2datatypes[12]);

	Method* pRoomType4_retChapterSysPrompt = new Method();
	pRoomType4_retChapterSysPrompt->name = TEXT("retChapterSysPrompt");
	pRoomType4_retChapterSysPrompt->methodUtype = 60;
	pRoomType4_retChapterSysPrompt->aliasID = 3;
	pRoomType4_retChapterSysPrompt->args = RoomType4_retChapterSysPrompt_args;

	pRoomType4Module->methods.Add(TEXT("retChapterSysPrompt"), pRoomType4_retChapterSysPrompt); 
	pRoomType4Module->useMethodDescrAlias = true;
	pRoomType4Module->idmethods.Add((uint16)pRoomType4_retChapterSysPrompt->aliasID, pRoomType4_retChapterSysPrompt);

	//DEBUG_MSG("EntityDef::initScriptModules: add(RoomType4), method(retChapterSysPrompt / 60).");

	TArray<DATATYPE_BASE*> RoomType4_retLocationIndexs_args;
	RoomType4_retLocationIndexs_args.Add(EntityDef::id2datatypes[1]);

	Method* pRoomType4_retLocationIndexs = new Method();
	pRoomType4_retLocationIndexs->name = TEXT("retLocationIndexs");
	pRoomType4_retLocationIndexs->methodUtype = 63;
	pRoomType4_retLocationIndexs->aliasID = 4;
	pRoomType4_retLocationIndexs->args = RoomType4_retLocationIndexs_args;

	pRoomType4Module->methods.Add(TEXT("retLocationIndexs"), pRoomType4_retLocationIndexs); 
	pRoomType4Module->useMethodDescrAlias = true;
	pRoomType4Module->idmethods.Add((uint16)pRoomType4_retLocationIndexs->aliasID, pRoomType4_retLocationIndexs);

	//DEBUG_MSG("EntityDef::initScriptModules: add(RoomType4), method(retLocationIndexs / 63).");

	TArray<DATATYPE_BASE*> RoomType4_retRoomBaseInfo_args;
	RoomType4_retRoomBaseInfo_args.Add(EntityDef::id2datatypes[1]);

	Method* pRoomType4_retRoomBaseInfo = new Method();
	pRoomType4_retRoomBaseInfo->name = TEXT("retRoomBaseInfo");
	pRoomType4_retRoomBaseInfo->methodUtype = 62;
	pRoomType4_retRoomBaseInfo->aliasID = 5;
	pRoomType4_retRoomBaseInfo->args = RoomType4_retRoomBaseInfo_args;

	pRoomType4Module->methods.Add(TEXT("retRoomBaseInfo"), pRoomType4_retRoomBaseInfo); 
	pRoomType4Module->useMethodDescrAlias = true;
	pRoomType4Module->idmethods.Add((uint16)pRoomType4_retRoomBaseInfo->aliasID, pRoomType4_retRoomBaseInfo);

	//DEBUG_MSG("EntityDef::initScriptModules: add(RoomType4), method(retRoomBaseInfo / 62).");

	TArray<DATATYPE_BASE*> RoomType4_updateMapping_args;
	RoomType4_updateMapping_args.Add(EntityDef::id2datatypes[1]);
	RoomType4_updateMapping_args.Add(EntityDef::id2datatypes[30]);

	Method* pRoomType4_updateMapping = new Method();
	pRoomType4_updateMapping->name = TEXT("updateMapping");
	pRoomType4_updateMapping->methodUtype = 51;
	pRoomType4_updateMapping->aliasID = 6;
	pRoomType4_updateMapping->args = RoomType4_updateMapping_args;

	pRoomType4Module->methods.Add(TEXT("updateMapping"), pRoomType4_updateMapping); 
	pRoomType4Module->useMethodDescrAlias = true;
	pRoomType4Module->idmethods.Add((uint16)pRoomType4_updateMapping->aliasID, pRoomType4_updateMapping);

	//DEBUG_MSG("EntityDef::initScriptModules: add(RoomType4), method(updateMapping / 51).");

	TArray<DATATYPE_BASE*> RoomType4_clientToBase_args;
	RoomType4_clientToBase_args.Add(EntityDef::id2datatypes[12]);

	Method* pRoomType4_clientToBase = new Method();
	pRoomType4_clientToBase->name = TEXT("clientToBase");
	pRoomType4_clientToBase->methodUtype = 59;
	pRoomType4_clientToBase->aliasID = -1;
	pRoomType4_clientToBase->args = RoomType4_clientToBase_args;

	pRoomType4Module->methods.Add(TEXT("clientToBase"), pRoomType4_clientToBase); 
	pRoomType4Module->base_methods.Add(TEXT("clientToBase"), pRoomType4_clientToBase);

	pRoomType4Module->idbase_methods.Add(pRoomType4_clientToBase->methodUtype, pRoomType4_clientToBase);

	//DEBUG_MSG("EntityDef::initScriptModules: add(RoomType4), method(clientToBase / 59).");

	TArray<DATATYPE_BASE*> RoomType4_clientReq_args;
	RoomType4_clientReq_args.Add(EntityDef::id2datatypes[1]);

	Method* pRoomType4_clientReq = new Method();
	pRoomType4_clientReq->name = TEXT("clientReq");
	pRoomType4_clientReq->methodUtype = 57;
	pRoomType4_clientReq->aliasID = -1;
	pRoomType4_clientReq->args = RoomType4_clientReq_args;

	pRoomType4Module->methods.Add(TEXT("clientReq"), pRoomType4_clientReq); 
	pRoomType4Module->cell_methods.Add(TEXT("clientReq"), pRoomType4_clientReq);

	pRoomType4Module->idcell_methods.Add(pRoomType4_clientReq->methodUtype, pRoomType4_clientReq);

	//DEBUG_MSG("EntityDef::initScriptModules: add(RoomType4), method(clientReq / 57).");

	TArray<DATATYPE_BASE*> RoomType4_onEnter_args;

	Method* pRoomType4_onEnter = new Method();
	pRoomType4_onEnter->name = TEXT("onEnter");
	pRoomType4_onEnter->methodUtype = 52;
	pRoomType4_onEnter->aliasID = -1;
	pRoomType4_onEnter->args = RoomType4_onEnter_args;

	pRoomType4Module->methods.Add(TEXT("onEnter"), pRoomType4_onEnter); 
	pRoomType4Module->cell_methods.Add(TEXT("onEnter"), pRoomType4_onEnter);

	pRoomType4Module->idcell_methods.Add(pRoomType4_onEnter->methodUtype, pRoomType4_onEnter);

	//DEBUG_MSG("EntityDef::initScriptModules: add(RoomType4), method(onEnter / 52).");

	TArray<DATATYPE_BASE*> RoomType4_playerOperation_args;
	RoomType4_playerOperation_args.Add(EntityDef::id2datatypes[1]);

	Method* pRoomType4_playerOperation = new Method();
	pRoomType4_playerOperation->name = TEXT("playerOperation");
	pRoomType4_playerOperation->methodUtype = 56;
	pRoomType4_playerOperation->aliasID = -1;
	pRoomType4_playerOperation->args = RoomType4_playerOperation_args;

	pRoomType4Module->methods.Add(TEXT("playerOperation"), pRoomType4_playerOperation); 
	pRoomType4Module->cell_methods.Add(TEXT("playerOperation"), pRoomType4_playerOperation);

	pRoomType4Module->idcell_methods.Add(pRoomType4_playerOperation->methodUtype, pRoomType4_playerOperation);

	//DEBUG_MSG("EntityDef::initScriptModules: add(RoomType4), method(playerOperation / 56).");

	TArray<DATATYPE_BASE*> RoomType4_reqRoomBaseInfo_args;

	Method* pRoomType4_reqRoomBaseInfo = new Method();
	pRoomType4_reqRoomBaseInfo->name = TEXT("reqRoomBaseInfo");
	pRoomType4_reqRoomBaseInfo->methodUtype = 55;
	pRoomType4_reqRoomBaseInfo->aliasID = -1;
	pRoomType4_reqRoomBaseInfo->args = RoomType4_reqRoomBaseInfo_args;

	pRoomType4Module->methods.Add(TEXT("reqRoomBaseInfo"), pRoomType4_reqRoomBaseInfo); 
	pRoomType4Module->cell_methods.Add(TEXT("reqRoomBaseInfo"), pRoomType4_reqRoomBaseInfo);

	pRoomType4Module->idcell_methods.Add(pRoomType4_reqRoomBaseInfo->methodUtype, pRoomType4_reqRoomBaseInfo);

	//DEBUG_MSG("EntityDef::initScriptModules: add(RoomType4), method(reqRoomBaseInfo / 55).");

	ScriptModule* pRoomType7Module = new ScriptModule("RoomType7", 9);
	EntityDef::moduledefs.Add(TEXT("RoomType7"), pRoomType7Module);
	EntityDef::idmoduledefs.Add(9, pRoomType7Module);

	Property* pRoomType7_position = new Property();
	pRoomType7_position->name = TEXT("position");
	pRoomType7_position->properUtype = 40000;
	pRoomType7_position->properFlags = 4;
	pRoomType7_position->aliasID = 1;
	KBVar* pRoomType7_position_defval = new KBVar(FVector());
	pRoomType7_position->pDefaultVal = pRoomType7_position_defval;
	pRoomType7Module->propertys.Add(TEXT("position"), pRoomType7_position); 

	pRoomType7Module->usePropertyDescrAlias = true;
	pRoomType7Module->idpropertys.Add((uint16)pRoomType7_position->aliasID, pRoomType7_position);

	//DEBUG_MSG("EntityDef::initScriptModules: add(RoomType7), property(position / 40000).");

	Property* pRoomType7_direction = new Property();
	pRoomType7_direction->name = TEXT("direction");
	pRoomType7_direction->properUtype = 40001;
	pRoomType7_direction->properFlags = 4;
	pRoomType7_direction->aliasID = 2;
	KBVar* pRoomType7_direction_defval = new KBVar(FVector());
	pRoomType7_direction->pDefaultVal = pRoomType7_direction_defval;
	pRoomType7Module->propertys.Add(TEXT("direction"), pRoomType7_direction); 

	pRoomType7Module->usePropertyDescrAlias = true;
	pRoomType7Module->idpropertys.Add((uint16)pRoomType7_direction->aliasID, pRoomType7_direction);

	//DEBUG_MSG("EntityDef::initScriptModules: add(RoomType7), property(direction / 40001).");

	Property* pRoomType7_spaceID = new Property();
	pRoomType7_spaceID->name = TEXT("spaceID");
	pRoomType7_spaceID->properUtype = 40002;
	pRoomType7_spaceID->properFlags = 16;
	pRoomType7_spaceID->aliasID = 3;
	KBVar* pRoomType7_spaceID_defval = new KBVar((uint32)FCString::Atoi64(TEXT("")));
	pRoomType7_spaceID->pDefaultVal = pRoomType7_spaceID_defval;
	pRoomType7Module->propertys.Add(TEXT("spaceID"), pRoomType7_spaceID); 

	pRoomType7Module->usePropertyDescrAlias = true;
	pRoomType7Module->idpropertys.Add((uint16)pRoomType7_spaceID->aliasID, pRoomType7_spaceID);

	//DEBUG_MSG("EntityDef::initScriptModules: add(RoomType7), property(spaceID / 40002).");

	TArray<DATATYPE_BASE*> RoomType7_cellToClient_args;
	RoomType7_cellToClient_args.Add(EntityDef::id2datatypes[1]);
	RoomType7_cellToClient_args.Add(EntityDef::id2datatypes[1]);

	Method* pRoomType7_cellToClient = new Method();
	pRoomType7_cellToClient->name = TEXT("cellToClient");
	pRoomType7_cellToClient->methodUtype = 78;
	pRoomType7_cellToClient->aliasID = 1;
	pRoomType7_cellToClient->args = RoomType7_cellToClient_args;

	pRoomType7Module->methods.Add(TEXT("cellToClient"), pRoomType7_cellToClient); 
	pRoomType7Module->useMethodDescrAlias = true;
	pRoomType7Module->idmethods.Add((uint16)pRoomType7_cellToClient->aliasID, pRoomType7_cellToClient);

	//DEBUG_MSG("EntityDef::initScriptModules: add(RoomType7), method(cellToClient / 78).");

	TArray<DATATYPE_BASE*> RoomType7_dealCardsToPlayer_args;
	RoomType7_dealCardsToPlayer_args.Add(EntityDef::id2datatypes[6]);
	RoomType7_dealCardsToPlayer_args.Add(EntityDef::id2datatypes[35]);

	Method* pRoomType7_dealCardsToPlayer = new Method();
	pRoomType7_dealCardsToPlayer->name = TEXT("dealCardsToPlayer");
	pRoomType7_dealCardsToPlayer->methodUtype = 75;
	pRoomType7_dealCardsToPlayer->aliasID = 2;
	pRoomType7_dealCardsToPlayer->args = RoomType7_dealCardsToPlayer_args;

	pRoomType7Module->methods.Add(TEXT("dealCardsToPlayer"), pRoomType7_dealCardsToPlayer); 
	pRoomType7Module->useMethodDescrAlias = true;
	pRoomType7Module->idmethods.Add((uint16)pRoomType7_dealCardsToPlayer->aliasID, pRoomType7_dealCardsToPlayer);

	//DEBUG_MSG("EntityDef::initScriptModules: add(RoomType7), method(dealCardsToPlayer / 75).");

	TArray<DATATYPE_BASE*> RoomType7_retChapterSysPrompt_args;
	RoomType7_retChapterSysPrompt_args.Add(EntityDef::id2datatypes[12]);

	Method* pRoomType7_retChapterSysPrompt = new Method();
	pRoomType7_retChapterSysPrompt->name = TEXT("retChapterSysPrompt");
	pRoomType7_retChapterSysPrompt->methodUtype = 74;
	pRoomType7_retChapterSysPrompt->aliasID = 3;
	pRoomType7_retChapterSysPrompt->args = RoomType7_retChapterSysPrompt_args;

	pRoomType7Module->methods.Add(TEXT("retChapterSysPrompt"), pRoomType7_retChapterSysPrompt); 
	pRoomType7Module->useMethodDescrAlias = true;
	pRoomType7Module->idmethods.Add((uint16)pRoomType7_retChapterSysPrompt->aliasID, pRoomType7_retChapterSysPrompt);

	//DEBUG_MSG("EntityDef::initScriptModules: add(RoomType7), method(retChapterSysPrompt / 74).");

	TArray<DATATYPE_BASE*> RoomType7_retLocationIndexs_args;
	RoomType7_retLocationIndexs_args.Add(EntityDef::id2datatypes[1]);

	Method* pRoomType7_retLocationIndexs = new Method();
	pRoomType7_retLocationIndexs->name = TEXT("retLocationIndexs");
	pRoomType7_retLocationIndexs->methodUtype = 77;
	pRoomType7_retLocationIndexs->aliasID = 4;
	pRoomType7_retLocationIndexs->args = RoomType7_retLocationIndexs_args;

	pRoomType7Module->methods.Add(TEXT("retLocationIndexs"), pRoomType7_retLocationIndexs); 
	pRoomType7Module->useMethodDescrAlias = true;
	pRoomType7Module->idmethods.Add((uint16)pRoomType7_retLocationIndexs->aliasID, pRoomType7_retLocationIndexs);

	//DEBUG_MSG("EntityDef::initScriptModules: add(RoomType7), method(retLocationIndexs / 77).");

	TArray<DATATYPE_BASE*> RoomType7_retRoomBaseInfo_args;
	RoomType7_retRoomBaseInfo_args.Add(EntityDef::id2datatypes[1]);

	Method* pRoomType7_retRoomBaseInfo = new Method();
	pRoomType7_retRoomBaseInfo->name = TEXT("retRoomBaseInfo");
	pRoomType7_retRoomBaseInfo->methodUtype = 76;
	pRoomType7_retRoomBaseInfo->aliasID = 5;
	pRoomType7_retRoomBaseInfo->args = RoomType7_retRoomBaseInfo_args;

	pRoomType7Module->methods.Add(TEXT("retRoomBaseInfo"), pRoomType7_retRoomBaseInfo); 
	pRoomType7Module->useMethodDescrAlias = true;
	pRoomType7Module->idmethods.Add((uint16)pRoomType7_retRoomBaseInfo->aliasID, pRoomType7_retRoomBaseInfo);

	//DEBUG_MSG("EntityDef::initScriptModules: add(RoomType7), method(retRoomBaseInfo / 76).");

	TArray<DATATYPE_BASE*> RoomType7_updateMapping_args;
	RoomType7_updateMapping_args.Add(EntityDef::id2datatypes[1]);
	RoomType7_updateMapping_args.Add(EntityDef::id2datatypes[33]);

	Method* pRoomType7_updateMapping = new Method();
	pRoomType7_updateMapping->name = TEXT("updateMapping");
	pRoomType7_updateMapping->methodUtype = 65;
	pRoomType7_updateMapping->aliasID = 6;
	pRoomType7_updateMapping->args = RoomType7_updateMapping_args;

	pRoomType7Module->methods.Add(TEXT("updateMapping"), pRoomType7_updateMapping); 
	pRoomType7Module->useMethodDescrAlias = true;
	pRoomType7Module->idmethods.Add((uint16)pRoomType7_updateMapping->aliasID, pRoomType7_updateMapping);

	//DEBUG_MSG("EntityDef::initScriptModules: add(RoomType7), method(updateMapping / 65).");

	TArray<DATATYPE_BASE*> RoomType7_clientToBase_args;
	RoomType7_clientToBase_args.Add(EntityDef::id2datatypes[12]);

	Method* pRoomType7_clientToBase = new Method();
	pRoomType7_clientToBase->name = TEXT("clientToBase");
	pRoomType7_clientToBase->methodUtype = 73;
	pRoomType7_clientToBase->aliasID = -1;
	pRoomType7_clientToBase->args = RoomType7_clientToBase_args;

	pRoomType7Module->methods.Add(TEXT("clientToBase"), pRoomType7_clientToBase); 
	pRoomType7Module->base_methods.Add(TEXT("clientToBase"), pRoomType7_clientToBase);

	pRoomType7Module->idbase_methods.Add(pRoomType7_clientToBase->methodUtype, pRoomType7_clientToBase);

	//DEBUG_MSG("EntityDef::initScriptModules: add(RoomType7), method(clientToBase / 73).");

	TArray<DATATYPE_BASE*> RoomType7_clientReq_args;
	RoomType7_clientReq_args.Add(EntityDef::id2datatypes[1]);

	Method* pRoomType7_clientReq = new Method();
	pRoomType7_clientReq->name = TEXT("clientReq");
	pRoomType7_clientReq->methodUtype = 71;
	pRoomType7_clientReq->aliasID = -1;
	pRoomType7_clientReq->args = RoomType7_clientReq_args;

	pRoomType7Module->methods.Add(TEXT("clientReq"), pRoomType7_clientReq); 
	pRoomType7Module->cell_methods.Add(TEXT("clientReq"), pRoomType7_clientReq);

	pRoomType7Module->idcell_methods.Add(pRoomType7_clientReq->methodUtype, pRoomType7_clientReq);

	//DEBUG_MSG("EntityDef::initScriptModules: add(RoomType7), method(clientReq / 71).");

	TArray<DATATYPE_BASE*> RoomType7_onEnter_args;

	Method* pRoomType7_onEnter = new Method();
	pRoomType7_onEnter->name = TEXT("onEnter");
	pRoomType7_onEnter->methodUtype = 66;
	pRoomType7_onEnter->aliasID = -1;
	pRoomType7_onEnter->args = RoomType7_onEnter_args;

	pRoomType7Module->methods.Add(TEXT("onEnter"), pRoomType7_onEnter); 
	pRoomType7Module->cell_methods.Add(TEXT("onEnter"), pRoomType7_onEnter);

	pRoomType7Module->idcell_methods.Add(pRoomType7_onEnter->methodUtype, pRoomType7_onEnter);

	//DEBUG_MSG("EntityDef::initScriptModules: add(RoomType7), method(onEnter / 66).");

	TArray<DATATYPE_BASE*> RoomType7_playerOperation_args;
	RoomType7_playerOperation_args.Add(EntityDef::id2datatypes[1]);

	Method* pRoomType7_playerOperation = new Method();
	pRoomType7_playerOperation->name = TEXT("playerOperation");
	pRoomType7_playerOperation->methodUtype = 70;
	pRoomType7_playerOperation->aliasID = -1;
	pRoomType7_playerOperation->args = RoomType7_playerOperation_args;

	pRoomType7Module->methods.Add(TEXT("playerOperation"), pRoomType7_playerOperation); 
	pRoomType7Module->cell_methods.Add(TEXT("playerOperation"), pRoomType7_playerOperation);

	pRoomType7Module->idcell_methods.Add(pRoomType7_playerOperation->methodUtype, pRoomType7_playerOperation);

	//DEBUG_MSG("EntityDef::initScriptModules: add(RoomType7), method(playerOperation / 70).");

	TArray<DATATYPE_BASE*> RoomType7_reqRoomBaseInfo_args;

	Method* pRoomType7_reqRoomBaseInfo = new Method();
	pRoomType7_reqRoomBaseInfo->name = TEXT("reqRoomBaseInfo");
	pRoomType7_reqRoomBaseInfo->methodUtype = 69;
	pRoomType7_reqRoomBaseInfo->aliasID = -1;
	pRoomType7_reqRoomBaseInfo->args = RoomType7_reqRoomBaseInfo_args;

	pRoomType7Module->methods.Add(TEXT("reqRoomBaseInfo"), pRoomType7_reqRoomBaseInfo); 
	pRoomType7Module->cell_methods.Add(TEXT("reqRoomBaseInfo"), pRoomType7_reqRoomBaseInfo);

	pRoomType7Module->idcell_methods.Add(pRoomType7_reqRoomBaseInfo->methodUtype, pRoomType7_reqRoomBaseInfo);

	//DEBUG_MSG("EntityDef::initScriptModules: add(RoomType7), method(reqRoomBaseInfo / 69).");

}

void EntityDef::initDefTypes()
{
	{
		uint16 utype = 2;
		FString typeName = TEXT("BOOL");
		FString name = TEXT("UINT8");
		DATATYPE_BASE** fPtr = EntityDef::datatypes.Find(name);
		DATATYPE_BASE* pVal = fPtr != NULL ? *fPtr : NULL;
		EntityDef::datatypes.Add(typeName, pVal);
		EntityDef::id2datatypes.Add(utype, EntityDef::datatypes[typeName]);
		EntityDef::datatype2id.Add(typeName, utype);
	}

	{
		uint16 utype = 3;
		FString typeName = TEXT("UINT16");
		FString name = TEXT("UINT16");
		DATATYPE_BASE** fPtr = EntityDef::datatypes.Find(name);
		DATATYPE_BASE* pVal = fPtr != NULL ? *fPtr : NULL;
		EntityDef::datatypes.Add(typeName, pVal);
		EntityDef::id2datatypes.Add(utype, EntityDef::datatypes[typeName]);
		EntityDef::datatype2id.Add(typeName, utype);
	}

	{
		uint16 utype = 5;
		FString typeName = TEXT("UID");
		FString name = TEXT("UINT64");
		DATATYPE_BASE** fPtr = EntityDef::datatypes.Find(name);
		DATATYPE_BASE* pVal = fPtr != NULL ? *fPtr : NULL;
		EntityDef::datatypes.Add(typeName, pVal);
		EntityDef::id2datatypes.Add(utype, EntityDef::datatypes[typeName]);
		EntityDef::datatype2id.Add(typeName, utype);
	}

	{
		uint16 utype = 4;
		FString typeName = TEXT("UINT32");
		FString name = TEXT("UINT32");
		DATATYPE_BASE** fPtr = EntityDef::datatypes.Find(name);
		DATATYPE_BASE* pVal = fPtr != NULL ? *fPtr : NULL;
		EntityDef::datatypes.Add(typeName, pVal);
		EntityDef::id2datatypes.Add(utype, EntityDef::datatypes[typeName]);
		EntityDef::datatype2id.Add(typeName, utype);
	}

	{
		uint16 utype = 6;
		FString typeName = TEXT("INT8");
		FString name = TEXT("INT8");
		DATATYPE_BASE** fPtr = EntityDef::datatypes.Find(name);
		DATATYPE_BASE* pVal = fPtr != NULL ? *fPtr : NULL;
		EntityDef::datatypes.Add(typeName, pVal);
		EntityDef::id2datatypes.Add(utype, EntityDef::datatypes[typeName]);
		EntityDef::datatype2id.Add(typeName, utype);
	}

	{
		uint16 utype = 7;
		FString typeName = TEXT("INT16");
		FString name = TEXT("INT16");
		DATATYPE_BASE** fPtr = EntityDef::datatypes.Find(name);
		DATATYPE_BASE* pVal = fPtr != NULL ? *fPtr : NULL;
		EntityDef::datatypes.Add(typeName, pVal);
		EntityDef::id2datatypes.Add(utype, EntityDef::datatypes[typeName]);
		EntityDef::datatype2id.Add(typeName, utype);
	}

	{
		uint16 utype = 8;
		FString typeName = TEXT("ENTITY_ID");
		FString name = TEXT("INT32");
		DATATYPE_BASE** fPtr = EntityDef::datatypes.Find(name);
		DATATYPE_BASE* pVal = fPtr != NULL ? *fPtr : NULL;
		EntityDef::datatypes.Add(typeName, pVal);
		EntityDef::id2datatypes.Add(utype, EntityDef::datatypes[typeName]);
		EntityDef::datatype2id.Add(typeName, utype);
	}

	{
		uint16 utype = 9;
		FString typeName = TEXT("INT64");
		FString name = TEXT("INT64");
		DATATYPE_BASE** fPtr = EntityDef::datatypes.Find(name);
		DATATYPE_BASE* pVal = fPtr != NULL ? *fPtr : NULL;
		EntityDef::datatypes.Add(typeName, pVal);
		EntityDef::id2datatypes.Add(utype, EntityDef::datatypes[typeName]);
		EntityDef::datatype2id.Add(typeName, utype);
	}

	{
		uint16 utype = 1;
		FString typeName = TEXT("STRING");
		FString name = TEXT("STRING");
		DATATYPE_BASE** fPtr = EntityDef::datatypes.Find(name);
		DATATYPE_BASE* pVal = fPtr != NULL ? *fPtr : NULL;
		EntityDef::datatypes.Add(typeName, pVal);
		EntityDef::id2datatypes.Add(utype, EntityDef::datatypes[typeName]);
		EntityDef::datatype2id.Add(typeName, utype);
	}

	{
		uint16 utype = 12;
		FString typeName = TEXT("UNICODE");
		FString name = TEXT("UNICODE");
		DATATYPE_BASE** fPtr = EntityDef::datatypes.Find(name);
		DATATYPE_BASE* pVal = fPtr != NULL ? *fPtr : NULL;
		EntityDef::datatypes.Add(typeName, pVal);
		EntityDef::id2datatypes.Add(utype, EntityDef::datatypes[typeName]);
		EntityDef::datatype2id.Add(typeName, utype);
	}

	{
		uint16 utype = 13;
		FString typeName = TEXT("FLOAT");
		FString name = TEXT("FLOAT");
		DATATYPE_BASE** fPtr = EntityDef::datatypes.Find(name);
		DATATYPE_BASE* pVal = fPtr != NULL ? *fPtr : NULL;
		EntityDef::datatypes.Add(typeName, pVal);
		EntityDef::id2datatypes.Add(utype, EntityDef::datatypes[typeName]);
		EntityDef::datatype2id.Add(typeName, utype);
	}

	{
		uint16 utype = 14;
		FString typeName = TEXT("DOUBLE");
		FString name = TEXT("DOUBLE");
		DATATYPE_BASE** fPtr = EntityDef::datatypes.Find(name);
		DATATYPE_BASE* pVal = fPtr != NULL ? *fPtr : NULL;
		EntityDef::datatypes.Add(typeName, pVal);
		EntityDef::id2datatypes.Add(utype, EntityDef::datatypes[typeName]);
		EntityDef::datatype2id.Add(typeName, utype);
	}

	{
		uint16 utype = 10;
		FString typeName = TEXT("PYTHON");
		FString name = TEXT("PYTHON");
		DATATYPE_BASE** fPtr = EntityDef::datatypes.Find(name);
		DATATYPE_BASE* pVal = fPtr != NULL ? *fPtr : NULL;
		EntityDef::datatypes.Add(typeName, pVal);
		EntityDef::id2datatypes.Add(utype, EntityDef::datatypes[typeName]);
		EntityDef::datatype2id.Add(typeName, utype);
	}

	{
		uint16 utype = 10;
		FString typeName = TEXT("PY_DICT");
		FString name = TEXT("PY_DICT");
		DATATYPE_BASE** fPtr = EntityDef::datatypes.Find(name);
		DATATYPE_BASE* pVal = fPtr != NULL ? *fPtr : NULL;
		EntityDef::datatypes.Add(typeName, pVal);
		EntityDef::id2datatypes.Add(utype, EntityDef::datatypes[typeName]);
		EntityDef::datatype2id.Add(typeName, utype);
	}

	{
		uint16 utype = 10;
		FString typeName = TEXT("PY_TUPLE");
		FString name = TEXT("PY_TUPLE");
		DATATYPE_BASE** fPtr = EntityDef::datatypes.Find(name);
		DATATYPE_BASE* pVal = fPtr != NULL ? *fPtr : NULL;
		EntityDef::datatypes.Add(typeName, pVal);
		EntityDef::id2datatypes.Add(utype, EntityDef::datatypes[typeName]);
		EntityDef::datatype2id.Add(typeName, utype);
	}

	{
		uint16 utype = 10;
		FString typeName = TEXT("PY_LIST");
		FString name = TEXT("PY_LIST");
		DATATYPE_BASE** fPtr = EntityDef::datatypes.Find(name);
		DATATYPE_BASE* pVal = fPtr != NULL ? *fPtr : NULL;
		EntityDef::datatypes.Add(typeName, pVal);
		EntityDef::id2datatypes.Add(utype, EntityDef::datatypes[typeName]);
		EntityDef::datatype2id.Add(typeName, utype);
	}

	{
		uint16 utype = 20;
		FString typeName = TEXT("ENTITYCALL");
		FString name = TEXT("ENTITYCALL");
		DATATYPE_BASE** fPtr = EntityDef::datatypes.Find(name);
		DATATYPE_BASE* pVal = fPtr != NULL ? *fPtr : NULL;
		EntityDef::datatypes.Add(typeName, pVal);
		EntityDef::id2datatypes.Add(utype, EntityDef::datatypes[typeName]);
		EntityDef::datatype2id.Add(typeName, utype);
	}

	{
		uint16 utype = 11;
		FString typeName = TEXT("BLOB");
		FString name = TEXT("BLOB");
		DATATYPE_BASE** fPtr = EntityDef::datatypes.Find(name);
		DATATYPE_BASE* pVal = fPtr != NULL ? *fPtr : NULL;
		EntityDef::datatypes.Add(typeName, pVal);
		EntityDef::id2datatypes.Add(utype, EntityDef::datatypes[typeName]);
		EntityDef::datatype2id.Add(typeName, utype);
	}

	{
		uint16 utype = 15;
		FString typeName = TEXT("VECTOR2");
		FString name = TEXT("VECTOR2");
		DATATYPE_BASE** fPtr = EntityDef::datatypes.Find(name);
		DATATYPE_BASE* pVal = fPtr != NULL ? *fPtr : NULL;
		EntityDef::datatypes.Add(typeName, pVal);
		EntityDef::id2datatypes.Add(utype, EntityDef::datatypes[typeName]);
		EntityDef::datatype2id.Add(typeName, utype);
	}

	{
		uint16 utype = 16;
		FString typeName = TEXT("VECTOR3");
		FString name = TEXT("VECTOR3");
		DATATYPE_BASE** fPtr = EntityDef::datatypes.Find(name);
		DATATYPE_BASE* pVal = fPtr != NULL ? *fPtr : NULL;
		EntityDef::datatypes.Add(typeName, pVal);
		EntityDef::id2datatypes.Add(utype, EntityDef::datatypes[typeName]);
		EntityDef::datatype2id.Add(typeName, utype);
	}

	{
		uint16 utype = 17;
		FString typeName = TEXT("VECTOR4");
		FString name = TEXT("VECTOR4");
		DATATYPE_BASE** fPtr = EntityDef::datatypes.Find(name);
		DATATYPE_BASE* pVal = fPtr != NULL ? *fPtr : NULL;
		EntityDef::datatypes.Add(typeName, pVal);
		EntityDef::id2datatypes.Add(utype, EntityDef::datatypes[typeName]);
		EntityDef::datatype2id.Add(typeName, utype);
	}

	{
		uint16 utype = 22;
		FString typeName = TEXT("ROOM_INFO");
		DATATYPE_ROOM_INFO* pDatatype = new DATATYPE_ROOM_INFO();
		EntityDef::datatypes.Add(typeName, (DATATYPE_BASE*)pDatatype);
		EntityDef::id2datatypes.Add(utype, EntityDef::datatypes[typeName]);
		EntityDef::datatype2id.Add(typeName, utype);
	}

	{
		uint16 utype = 23;
		FString typeName = TEXT("AnonymousArray_23");
		DATATYPE_AnonymousArray_23* pDatatype = new DATATYPE_AnonymousArray_23();
		EntityDef::datatypes.Add(typeName, (DATATYPE_BASE*)pDatatype);
		EntityDef::id2datatypes.Add(utype, EntityDef::datatypes[typeName]);
		EntityDef::datatype2id.Add(typeName, utype);
	}

	{
		uint16 utype = 24;
		FString typeName = TEXT("AnonymousArray_24");
		DATATYPE_AnonymousArray_24* pDatatype = new DATATYPE_AnonymousArray_24();
		EntityDef::datatypes.Add(typeName, (DATATYPE_BASE*)pDatatype);
		EntityDef::id2datatypes.Add(utype, EntityDef::datatypes[typeName]);
		EntityDef::datatype2id.Add(typeName, utype);
	}

	{
		uint16 utype = 26;
		FString typeName = TEXT("AnonymousArray_26");
		DATATYPE_AnonymousArray_26* pDatatype = new DATATYPE_AnonymousArray_26();
		EntityDef::datatypes.Add(typeName, (DATATYPE_BASE*)pDatatype);
		EntityDef::id2datatypes.Add(utype, EntityDef::datatypes[typeName]);
		EntityDef::datatype2id.Add(typeName, utype);
	}

	{
		uint16 utype = 27;
		FString typeName = TEXT("AnonymousArray_27");
		DATATYPE_AnonymousArray_27* pDatatype = new DATATYPE_AnonymousArray_27();
		EntityDef::datatypes.Add(typeName, (DATATYPE_BASE*)pDatatype);
		EntityDef::id2datatypes.Add(utype, EntityDef::datatypes[typeName]);
		EntityDef::datatype2id.Add(typeName, utype);
	}

	{
		uint16 utype = 29;
		FString typeName = TEXT("AnonymousArray_29");
		DATATYPE_AnonymousArray_29* pDatatype = new DATATYPE_AnonymousArray_29();
		EntityDef::datatypes.Add(typeName, (DATATYPE_BASE*)pDatatype);
		EntityDef::id2datatypes.Add(utype, EntityDef::datatypes[typeName]);
		EntityDef::datatype2id.Add(typeName, utype);
	}

	{
		uint16 utype = 30;
		FString typeName = TEXT("AnonymousArray_30");
		DATATYPE_AnonymousArray_30* pDatatype = new DATATYPE_AnonymousArray_30();
		EntityDef::datatypes.Add(typeName, (DATATYPE_BASE*)pDatatype);
		EntityDef::id2datatypes.Add(utype, EntityDef::datatypes[typeName]);
		EntityDef::datatype2id.Add(typeName, utype);
	}

	{
		uint16 utype = 32;
		FString typeName = TEXT("AnonymousArray_32");
		DATATYPE_AnonymousArray_32* pDatatype = new DATATYPE_AnonymousArray_32();
		EntityDef::datatypes.Add(typeName, (DATATYPE_BASE*)pDatatype);
		EntityDef::id2datatypes.Add(utype, EntityDef::datatypes[typeName]);
		EntityDef::datatype2id.Add(typeName, utype);
	}

	{
		uint16 utype = 33;
		FString typeName = TEXT("AnonymousArray_33");
		DATATYPE_AnonymousArray_33* pDatatype = new DATATYPE_AnonymousArray_33();
		EntityDef::datatypes.Add(typeName, (DATATYPE_BASE*)pDatatype);
		EntityDef::id2datatypes.Add(utype, EntityDef::datatypes[typeName]);
		EntityDef::datatype2id.Add(typeName, utype);
	}

	{
		uint16 utype = 35;
		FString typeName = TEXT("AnonymousArray_35");
		DATATYPE_AnonymousArray_35* pDatatype = new DATATYPE_AnonymousArray_35();
		EntityDef::datatypes.Add(typeName, (DATATYPE_BASE*)pDatatype);
		EntityDef::id2datatypes.Add(utype, EntityDef::datatypes[typeName]);
		EntityDef::datatype2id.Add(typeName, utype);
	}

	for(auto& Elem : EntityDef::datatypes)
	{
		if(Elem.Value)
		{
			Elem.Value->bind();
		}
	}
}

