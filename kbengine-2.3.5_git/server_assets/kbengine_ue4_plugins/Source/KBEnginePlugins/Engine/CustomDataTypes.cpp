#include "CustomDataTypes.h"
#include "EntityDef.h"
#include "KBDebug.h"
#include "DataTypes.h"
#include "Runtime/Core/Public/Misc/Variant.h"

void DATATYPE_ROOM_INFO::createFromStreamEx(MemoryStream& stream, ROOM_INFO& datas)
{
	datas.chapterNum = stream.readUint8();
	datas.payType = stream.readUint8();
}

void DATATYPE_ROOM_INFO::addToStreamEx(Bundle& stream, const ROOM_INFO& v)
{
	stream.writeUint8(v.chapterNum);
	stream.writeUint8(v.payType);
}

void DATATYPE_AnonymousArray_23::createFromStreamEx(MemoryStream& stream, TArray<uint8>& datas)
{
	uint32 size = stream.readUint32();
	while(size > 0)
	{
		--size;
		datas.Add(stream.readUint8());
	};

}

void DATATYPE_AnonymousArray_23::addToStreamEx(Bundle& stream, const TArray<uint8>& v)
{
	stream.writeUint32((uint32)v.Num());
	for(int i=0; i<v.Num(); ++i)
	{
		stream.writeUint8(v[i]);
	};
}

void DATATYPE_AnonymousArray_24::createFromStreamEx(MemoryStream& stream, TArray<TArray<uint8>>& datas)
{
	return itemType.createFromStreamEx(stream);
}

void DATATYPE_AnonymousArray_24::addToStreamEx(Bundle& stream, const TArray<TArray<uint8>>& v)
{
	itemType.addToStreamEx(stream, v);
}
};

void DATATYPE_AnonymousArray_26::createFromStreamEx(MemoryStream& stream, TArray<uint8>& datas)
{
	uint32 size = stream.readUint32();
	while(size > 0)
	{
		--size;
		datas.Add(stream.readUint8());
	};

}

void DATATYPE_AnonymousArray_26::addToStreamEx(Bundle& stream, const TArray<uint8>& v)
{
	stream.writeUint32((uint32)v.Num());
	for(int i=0; i<v.Num(); ++i)
	{
		stream.writeUint8(v[i]);
	};
}

void DATATYPE_AnonymousArray_27::createFromStreamEx(MemoryStream& stream, TArray<TArray<uint8>>& datas)
{
	return itemType.createFromStreamEx(stream);
}

void DATATYPE_AnonymousArray_27::addToStreamEx(Bundle& stream, const TArray<TArray<uint8>>& v)
{
	itemType.addToStreamEx(stream, v);
}
};

void DATATYPE_AnonymousArray_29::createFromStreamEx(MemoryStream& stream, TArray<uint8>& datas)
{
	uint32 size = stream.readUint32();
	while(size > 0)
	{
		--size;
		datas.Add(stream.readUint8());
	};

}

void DATATYPE_AnonymousArray_29::addToStreamEx(Bundle& stream, const TArray<uint8>& v)
{
	stream.writeUint32((uint32)v.Num());
	for(int i=0; i<v.Num(); ++i)
	{
		stream.writeUint8(v[i]);
	};
}

void DATATYPE_AnonymousArray_30::createFromStreamEx(MemoryStream& stream, TArray<TArray<uint8>>& datas)
{
	return itemType.createFromStreamEx(stream);
}

void DATATYPE_AnonymousArray_30::addToStreamEx(Bundle& stream, const TArray<TArray<uint8>>& v)
{
	itemType.addToStreamEx(stream, v);
}
};

void DATATYPE_AnonymousArray_32::createFromStreamEx(MemoryStream& stream, TArray<uint8>& datas)
{
	uint32 size = stream.readUint32();
	while(size > 0)
	{
		--size;
		datas.Add(stream.readUint8());
	};

}

void DATATYPE_AnonymousArray_32::addToStreamEx(Bundle& stream, const TArray<uint8>& v)
{
	stream.writeUint32((uint32)v.Num());
	for(int i=0; i<v.Num(); ++i)
	{
		stream.writeUint8(v[i]);
	};
}

void DATATYPE_AnonymousArray_33::createFromStreamEx(MemoryStream& stream, TArray<TArray<uint8>>& datas)
{
	return itemType.createFromStreamEx(stream);
}

void DATATYPE_AnonymousArray_33::addToStreamEx(Bundle& stream, const TArray<TArray<uint8>>& v)
{
	itemType.addToStreamEx(stream, v);
}
};

void DATATYPE_AnonymousArray_35::createFromStreamEx(MemoryStream& stream, TArray<uint8>& datas)
{
	uint32 size = stream.readUint32();
	while(size > 0)
	{
		--size;
		datas.Add(stream.readUint8());
	};

}

void DATATYPE_AnonymousArray_35::addToStreamEx(Bundle& stream, const TArray<uint8>& v)
{
	stream.writeUint32((uint32)v.Num());
	for(int i=0; i<v.Num(); ++i)
	{
		stream.writeUint8(v[i]);
	};
}

