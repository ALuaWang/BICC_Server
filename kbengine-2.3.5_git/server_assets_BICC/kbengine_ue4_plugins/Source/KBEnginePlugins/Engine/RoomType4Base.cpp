#include "RoomType4Base.h"
#include "KBVar.h"
#include "EntityDef.h"
#include "ScriptModule.h"
#include "Property.h"
#include "Method.h"
#include "DataTypes.h"
#include "CustomDataTypes.h"
#include "MemoryStream.h"
#include "EntityComponent.h"


void RoomType4Base::onGetBase()
{
	if(pBaseEntityCall)
		delete pBaseEntityCall;

	pBaseEntityCall = new EntityBaseEntityCall_RoomType4Base(id(), className());
}

void RoomType4Base::onGetCell()
{
	if(pCellEntityCall)
		delete pCellEntityCall;

	pCellEntityCall = new EntityCellEntityCall_RoomType4Base(id(), className());
}

void RoomType4Base::onLoseCell()
{
	delete pCellEntityCall;
	pCellEntityCall = NULL;
}

EntityCall* RoomType4Base::getBaseEntityCall()
{
	return pBaseEntityCall;
}

EntityCall* RoomType4Base::getCellEntityCall()
{
	return pCellEntityCall;
}

void RoomType4Base::onRemoteMethodCall(MemoryStream& stream)
{
	ScriptModule* sm = *EntityDef::moduledefs.Find("RoomType4");
	uint16 methodUtype = 0;
	uint16 componentPropertyUType = 0;

	if (sm->useMethodDescrAlias)
	{
		componentPropertyUType = stream.readUint8();
		methodUtype = stream.read<uint8>();
	}
	else
	{
		componentPropertyUType = stream.readUint16();
		methodUtype = stream.read<uint16>();
	}

	if(componentPropertyUType > 0)
	{
		KBE_ASSERT(false);

		return;
	}

	Method* pMethod = sm->idmethods[methodUtype];

	switch(pMethod->methodUtype)
	{
		case 64:
		{
			FString cellToClient_arg1 = stream.readString();
			FString cellToClient_arg2 = stream.readString();
			cellToClient(cellToClient_arg1, cellToClient_arg2);
			break;
		}
		case 61:
		{
			int8 dealCardsToPlayer_arg1 = stream.readInt8();
			TArray<uint8> dealCardsToPlayer_arg2;
			((DATATYPE_AnonymousArray_32*)pMethod->args[1]).createFromStreamEx(stream, dealCardsToPlayer_arg2);
			dealCardsToPlayer(dealCardsToPlayer_arg1, dealCardsToPlayer_arg2);
			break;
		}
		case 60:
		{
			FString retChapterSysPrompt_arg1 = stream.readUnicode();
			retChapterSysPrompt(retChapterSysPrompt_arg1);
			break;
		}
		case 63:
		{
			FString retLocationIndexs_arg1 = stream.readString();
			retLocationIndexs(retLocationIndexs_arg1);
			break;
		}
		case 62:
		{
			FString retRoomBaseInfo_arg1 = stream.readString();
			retRoomBaseInfo(retRoomBaseInfo_arg1);
			break;
		}
		case 51:
		{
			FString updateMapping_arg1 = stream.readString();
			TArray<TArray<uint8>> updateMapping_arg2;
			((DATATYPE_AnonymousArray_30*)pMethod->args[1]).createFromStreamEx(stream, updateMapping_arg2);
			updateMapping(updateMapping_arg1, updateMapping_arg2);
			break;
		}
		default:
			break;
	};
}

void RoomType4Base::onUpdatePropertys(MemoryStream& stream)
{
	ScriptModule* sm = *EntityDef::moduledefs.Find("RoomType4");

	while(stream.length() > 0)
	{
		uint16 componentPropertyUType = 0;
		uint16 properUtype = 0;

		if (sm->usePropertyDescrAlias)
		{
			componentPropertyUType = stream.readUint8();
			properUtype = stream.read<uint8>();
		}
		else
		{
			componentPropertyUType = stream.readUint16();
			properUtype = stream.read<uint16>();
		}

		if(componentPropertyUType > 0)
		{
			KBE_ASSERT(false);

			return;
		}

		Property* pProp = sm->idpropertys[properUtype];

		switch(pProp->properUtype)
		{
			case 40001:
			{
				FVector oldval_direction = direction;
				direction = stream.readVector3();

				if(pProp->isBase())
				{
					if(inited())
						onDirectionChanged(oldval_direction);
				}
				else
				{
					if(inWorld())
						onDirectionChanged(oldval_direction);
				}

				break;
			}
			case 40000:
			{
				FVector oldval_position = position;
				position = stream.readVector3();

				if(pProp->isBase())
				{
					if(inited())
						onPositionChanged(oldval_position);
				}
				else
				{
					if(inWorld())
						onPositionChanged(oldval_position);
				}

				break;
			}
			case 40002:
			{
				stream.readUint32();
				break;
			}
			default:
				break;
		};
	}
}

void RoomType4Base::callPropertysSetMethods()
{
	ScriptModule* sm = EntityDef::moduledefs["RoomType4"];
	TMap<uint16, Property*>& pdatas = sm->idpropertys;

	FVector oldval_direction = direction;
	Property* pProp_direction = pdatas[2];
	if(pProp_direction->isBase())
	{
		if(inited() && !inWorld())
			onDirectionChanged(oldval_direction);
	}
	else
	{
		if(inWorld())
		{
			if(pProp_direction->isOwnerOnly() && !isPlayer())
			{
			}
			else
			{
				onDirectionChanged(oldval_direction);
			}
		}
	}

	FVector oldval_position = position;
	Property* pProp_position = pdatas[1];
	if(pProp_position->isBase())
	{
		if(inited() && !inWorld())
			onPositionChanged(oldval_position);
	}
	else
	{
		if(inWorld())
		{
			if(pProp_position->isOwnerOnly() && !isPlayer())
			{
			}
			else
			{
				onPositionChanged(oldval_position);
			}
		}
	}

}

RoomType4Base::RoomType4Base():
	Entity(),
	pBaseEntityCall(NULL),
	pCellEntityCall(NULL)
{
}

RoomType4Base::~RoomType4Base()
{
	if(pBaseEntityCall)
		delete pBaseEntityCall;

	if(pCellEntityCall)
		delete pCellEntityCall;

}

void RoomType4Base::attachComponents()
{
}

void RoomType4Base::detachComponents()
{
}

