#include "EntityCallRoomType4Base.h"
#include "Bundle.h"


EntityBaseEntityCall_RoomType4Base::EntityBaseEntityCall_RoomType4Base(int32 eid, const FString& ename) : EntityCall(eid, ename)
{
	type = ENTITYCALL_TYPE_BASE;
}

EntityBaseEntityCall_RoomType4Base::~EntityBaseEntityCall_RoomType4Base()
{
}

void EntityBaseEntityCall_RoomType4Base::clientToBase(const FString& arg1)
{
	Bundle* pBundleRet = newCall("clientToBase", 0);
	if(!pBundleRet)
		return;

	pBundleRet->writeUnicode(arg1);
	sendCall(NULL);
}



EntityCellEntityCall_RoomType4Base::EntityCellEntityCall_RoomType4Base(int32 eid, const FString& ename) : EntityCall(eid, ename)
{
	type = ENTITYCALL_TYPE_CELL;
}

EntityCellEntityCall_RoomType4Base::~EntityCellEntityCall_RoomType4Base()
{
}

void EntityCellEntityCall_RoomType4Base::clientReq(const FString& arg1)
{
	Bundle* pBundleRet = newCall("clientReq", 0);
	if(!pBundleRet)
		return;

	pBundleRet->writeString(arg1);
	sendCall(NULL);
}

void EntityCellEntityCall_RoomType4Base::onEnter()
{
	Bundle* pBundleRet = newCall("onEnter", 0);
	if(!pBundleRet)
		return;

	sendCall(NULL);
}

void EntityCellEntityCall_RoomType4Base::playerOperation(const FString& arg1)
{
	Bundle* pBundleRet = newCall("playerOperation", 0);
	if(!pBundleRet)
		return;

	pBundleRet->writeString(arg1);
	sendCall(NULL);
}

void EntityCellEntityCall_RoomType4Base::reqRoomBaseInfo()
{
	Bundle* pBundleRet = newCall("reqRoomBaseInfo", 0);
	if(!pBundleRet)
		return;

	sendCall(NULL);
}

