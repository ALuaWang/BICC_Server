#include "EntityCallRoomType3Base.h"
#include "Bundle.h"


EntityBaseEntityCall_RoomType3Base::EntityBaseEntityCall_RoomType3Base(int32 eid, const FString& ename) : EntityCall(eid, ename)
{
	type = ENTITYCALL_TYPE_BASE;
}

EntityBaseEntityCall_RoomType3Base::~EntityBaseEntityCall_RoomType3Base()
{
}

void EntityBaseEntityCall_RoomType3Base::clientToBase(const FString& arg1)
{
	Bundle* pBundleRet = newCall("clientToBase", 0);
	if(!pBundleRet)
		return;

	pBundleRet->writeUnicode(arg1);
	sendCall(NULL);
}



EntityCellEntityCall_RoomType3Base::EntityCellEntityCall_RoomType3Base(int32 eid, const FString& ename) : EntityCall(eid, ename)
{
	type = ENTITYCALL_TYPE_CELL;
}

EntityCellEntityCall_RoomType3Base::~EntityCellEntityCall_RoomType3Base()
{
}

void EntityCellEntityCall_RoomType3Base::clientReq(const FString& arg1)
{
	Bundle* pBundleRet = newCall("clientReq", 0);
	if(!pBundleRet)
		return;

	pBundleRet->writeString(arg1);
	sendCall(NULL);
}

void EntityCellEntityCall_RoomType3Base::onEnter()
{
	Bundle* pBundleRet = newCall("onEnter", 0);
	if(!pBundleRet)
		return;

	sendCall(NULL);
}

void EntityCellEntityCall_RoomType3Base::playerOperation(const FString& arg1)
{
	Bundle* pBundleRet = newCall("playerOperation", 0);
	if(!pBundleRet)
		return;

	pBundleRet->writeString(arg1);
	sendCall(NULL);
}

void EntityCellEntityCall_RoomType3Base::reqRoomBaseInfo()
{
	Bundle* pBundleRet = newCall("reqRoomBaseInfo", 0);
	if(!pBundleRet)
		return;

	sendCall(NULL);
}

