#include "EntityCallRoomType7Base.h"
#include "Bundle.h"


EntityBaseEntityCall_RoomType7Base::EntityBaseEntityCall_RoomType7Base(int32 eid, const FString& ename) : EntityCall(eid, ename)
{
	type = ENTITYCALL_TYPE_BASE;
}

EntityBaseEntityCall_RoomType7Base::~EntityBaseEntityCall_RoomType7Base()
{
}

void EntityBaseEntityCall_RoomType7Base::clientToBase(const FString& arg1)
{
	Bundle* pBundleRet = newCall("clientToBase", 0);
	if(!pBundleRet)
		return;

	pBundleRet->writeUnicode(arg1);
	sendCall(NULL);
}



EntityCellEntityCall_RoomType7Base::EntityCellEntityCall_RoomType7Base(int32 eid, const FString& ename) : EntityCall(eid, ename)
{
	type = ENTITYCALL_TYPE_CELL;
}

EntityCellEntityCall_RoomType7Base::~EntityCellEntityCall_RoomType7Base()
{
}

void EntityCellEntityCall_RoomType7Base::clientReq(const FString& arg1)
{
	Bundle* pBundleRet = newCall("clientReq", 0);
	if(!pBundleRet)
		return;

	pBundleRet->writeString(arg1);
	sendCall(NULL);
}

void EntityCellEntityCall_RoomType7Base::onEnter()
{
	Bundle* pBundleRet = newCall("onEnter", 0);
	if(!pBundleRet)
		return;

	sendCall(NULL);
}

void EntityCellEntityCall_RoomType7Base::playerOperation(const FString& arg1)
{
	Bundle* pBundleRet = newCall("playerOperation", 0);
	if(!pBundleRet)
		return;

	pBundleRet->writeString(arg1);
	sendCall(NULL);
}

void EntityCellEntityCall_RoomType7Base::reqRoomBaseInfo()
{
	Bundle* pBundleRet = newCall("reqRoomBaseInfo", 0);
	if(!pBundleRet)
		return;

	sendCall(NULL);
}

