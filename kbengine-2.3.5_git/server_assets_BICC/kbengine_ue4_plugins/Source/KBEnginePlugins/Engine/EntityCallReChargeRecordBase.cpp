#include "EntityCallReChargeRecordBase.h"
#include "Bundle.h"


EntityBaseEntityCall_ReChargeRecordBase::EntityBaseEntityCall_ReChargeRecordBase(int32 eid, const FString& ename) : EntityCall(eid, ename)
{
	type = ENTITYCALL_TYPE_BASE;
}

EntityBaseEntityCall_ReChargeRecordBase::~EntityBaseEntityCall_ReChargeRecordBase()
{
}



EntityCellEntityCall_ReChargeRecordBase::EntityCellEntityCall_ReChargeRecordBase(int32 eid, const FString& ename) : EntityCall(eid, ename)
{
	type = ENTITYCALL_TYPE_CELL;
}

EntityCellEntityCall_ReChargeRecordBase::~EntityCellEntityCall_ReChargeRecordBase()
{
}

