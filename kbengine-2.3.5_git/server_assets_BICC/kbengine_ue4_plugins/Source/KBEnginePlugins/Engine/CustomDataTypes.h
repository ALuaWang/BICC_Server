/*
	Generated by KBEngine!
	Please do not modify this file!
	tools = kbcmd
*/

#pragma once
#include "KBECommon.h"
#include "KBETypes.h"
#include "MemoryStream.h"
#include "Bundle.h"
#include "DataTypes.h"


class KBENGINEPLUGINS_API DATATYPE_ROOM_INFO : DATATYPE_BASE
{
public:
	void createFromStreamEx(MemoryStream& stream, ROOM_INFO& datas);
	void addToStreamEx(Bundle& stream, const ROOM_INFO& v);
};


class KBENGINEPLUGINS_API DATATYPE_AnonymousArray_23 : DATATYPE_BASE
{
public:
	void createFromStreamEx(MemoryStream& stream, TArray<uint8>& datas);
	void addToStreamEx(Bundle& stream, const TArray<uint8>& v);
};


class KBENGINEPLUGINS_API DATATYPE_AnonymousArray_24 : DATATYPE_BASE
{
public:
	class KBENGINEPLUGINS_API DATATYPE_AnonymousArray_24_ChildArray : public DATATYPE_BASE
	{
	public:
		class KBENGINEPLUGINS_API DATATYPE__RoomType2_updateMapping_ArrayType_ChildArray : public DATATYPE_BASE
		{
		public:
			void createFromStreamEx(MemoryStream& stream, TArray<uint8>& datas)
			{
				uint32 size = stream.readUint32();
				while(size > 0)
				{
					--size;
					datas.Add(stream.readUint8());
				};

			}

			void addToStreamEx(Bundle& stream, const TArray<uint8>& v)
			{
				stream.writeUint32((uint32)v.Num());
				for(int i=0; i<v.Num(); ++i)
				{
					stream.writeUint8(v[i]);
				};
			}
		};

		DATATYPE__RoomType2_updateMapping_ArrayType_ChildArray itemType;

		void createFromStreamEx(MemoryStream& stream, TArray<TArray<uint8>>& datas)
		{
			uint32 size = stream.readUint32();
			while(size > 0)
			{
				--size;
				itemType.createFromStreamEx(stream, datas.EmplaceAt_GetRef(datas.Num()));
			};

		}

		void addToStreamEx(Bundle& stream, const TArray<TArray<uint8>>& v)
		{
			stream.writeUint32((uint32)v.Num());
			for(int i=0; i<v.Num(); ++i)
			{
				itemType.addToStreamEx(stream, v[i]);
			};
		}
	};

	DATATYPE_AnonymousArray_24_ChildArray itemType;

	void createFromStreamEx(MemoryStream& stream, TArray<TArray<uint8>>& datas);
	void addToStreamEx(Bundle& stream, const TArray<TArray<uint8>>& v);

class KBENGINEPLUGINS_API DATATYPE_AnonymousArray_26 : DATATYPE_BASE
{
public:
	void createFromStreamEx(MemoryStream& stream, TArray<uint8>& datas);
	void addToStreamEx(Bundle& stream, const TArray<uint8>& v);
};


class KBENGINEPLUGINS_API DATATYPE_AnonymousArray_27 : DATATYPE_BASE
{
public:
	class KBENGINEPLUGINS_API DATATYPE_AnonymousArray_27_ChildArray : public DATATYPE_BASE
	{
	public:
		class KBENGINEPLUGINS_API DATATYPE__RoomType3_updateMapping_ArrayType_ChildArray : public DATATYPE_BASE
		{
		public:
			void createFromStreamEx(MemoryStream& stream, TArray<uint8>& datas)
			{
				uint32 size = stream.readUint32();
				while(size > 0)
				{
					--size;
					datas.Add(stream.readUint8());
				};

			}

			void addToStreamEx(Bundle& stream, const TArray<uint8>& v)
			{
				stream.writeUint32((uint32)v.Num());
				for(int i=0; i<v.Num(); ++i)
				{
					stream.writeUint8(v[i]);
				};
			}
		};

		DATATYPE__RoomType3_updateMapping_ArrayType_ChildArray itemType;

		void createFromStreamEx(MemoryStream& stream, TArray<TArray<uint8>>& datas)
		{
			uint32 size = stream.readUint32();
			while(size > 0)
			{
				--size;
				itemType.createFromStreamEx(stream, datas.EmplaceAt_GetRef(datas.Num()));
			};

		}

		void addToStreamEx(Bundle& stream, const TArray<TArray<uint8>>& v)
		{
			stream.writeUint32((uint32)v.Num());
			for(int i=0; i<v.Num(); ++i)
			{
				itemType.addToStreamEx(stream, v[i]);
			};
		}
	};

	DATATYPE_AnonymousArray_27_ChildArray itemType;

	void createFromStreamEx(MemoryStream& stream, TArray<TArray<uint8>>& datas);
	void addToStreamEx(Bundle& stream, const TArray<TArray<uint8>>& v);

class KBENGINEPLUGINS_API DATATYPE_AnonymousArray_29 : DATATYPE_BASE
{
public:
	void createFromStreamEx(MemoryStream& stream, TArray<uint8>& datas);
	void addToStreamEx(Bundle& stream, const TArray<uint8>& v);
};


class KBENGINEPLUGINS_API DATATYPE_AnonymousArray_30 : DATATYPE_BASE
{
public:
	class KBENGINEPLUGINS_API DATATYPE_AnonymousArray_30_ChildArray : public DATATYPE_BASE
	{
	public:
		class KBENGINEPLUGINS_API DATATYPE__RoomType4_updateMapping_ArrayType_ChildArray : public DATATYPE_BASE
		{
		public:
			void createFromStreamEx(MemoryStream& stream, TArray<uint8>& datas)
			{
				uint32 size = stream.readUint32();
				while(size > 0)
				{
					--size;
					datas.Add(stream.readUint8());
				};

			}

			void addToStreamEx(Bundle& stream, const TArray<uint8>& v)
			{
				stream.writeUint32((uint32)v.Num());
				for(int i=0; i<v.Num(); ++i)
				{
					stream.writeUint8(v[i]);
				};
			}
		};

		DATATYPE__RoomType4_updateMapping_ArrayType_ChildArray itemType;

		void createFromStreamEx(MemoryStream& stream, TArray<TArray<uint8>>& datas)
		{
			uint32 size = stream.readUint32();
			while(size > 0)
			{
				--size;
				itemType.createFromStreamEx(stream, datas.EmplaceAt_GetRef(datas.Num()));
			};

		}

		void addToStreamEx(Bundle& stream, const TArray<TArray<uint8>>& v)
		{
			stream.writeUint32((uint32)v.Num());
			for(int i=0; i<v.Num(); ++i)
			{
				itemType.addToStreamEx(stream, v[i]);
			};
		}
	};

	DATATYPE_AnonymousArray_30_ChildArray itemType;

	void createFromStreamEx(MemoryStream& stream, TArray<TArray<uint8>>& datas);
	void addToStreamEx(Bundle& stream, const TArray<TArray<uint8>>& v);

class KBENGINEPLUGINS_API DATATYPE_AnonymousArray_32 : DATATYPE_BASE
{
public:
	void createFromStreamEx(MemoryStream& stream, TArray<uint8>& datas);
	void addToStreamEx(Bundle& stream, const TArray<uint8>& v);
};


class KBENGINEPLUGINS_API DATATYPE_AnonymousArray_33 : DATATYPE_BASE
{
public:
	class KBENGINEPLUGINS_API DATATYPE_AnonymousArray_33_ChildArray : public DATATYPE_BASE
	{
	public:
		class KBENGINEPLUGINS_API DATATYPE__RoomType7_updateMapping_ArrayType_ChildArray : public DATATYPE_BASE
		{
		public:
			void createFromStreamEx(MemoryStream& stream, TArray<uint8>& datas)
			{
				uint32 size = stream.readUint32();
				while(size > 0)
				{
					--size;
					datas.Add(stream.readUint8());
				};

			}

			void addToStreamEx(Bundle& stream, const TArray<uint8>& v)
			{
				stream.writeUint32((uint32)v.Num());
				for(int i=0; i<v.Num(); ++i)
				{
					stream.writeUint8(v[i]);
				};
			}
		};

		DATATYPE__RoomType7_updateMapping_ArrayType_ChildArray itemType;

		void createFromStreamEx(MemoryStream& stream, TArray<TArray<uint8>>& datas)
		{
			uint32 size = stream.readUint32();
			while(size > 0)
			{
				--size;
				itemType.createFromStreamEx(stream, datas.EmplaceAt_GetRef(datas.Num()));
			};

		}

		void addToStreamEx(Bundle& stream, const TArray<TArray<uint8>>& v)
		{
			stream.writeUint32((uint32)v.Num());
			for(int i=0; i<v.Num(); ++i)
			{
				itemType.addToStreamEx(stream, v[i]);
			};
		}
	};

	DATATYPE_AnonymousArray_33_ChildArray itemType;

	void createFromStreamEx(MemoryStream& stream, TArray<TArray<uint8>>& datas);
	void addToStreamEx(Bundle& stream, const TArray<TArray<uint8>>& v);

class KBENGINEPLUGINS_API DATATYPE_AnonymousArray_35 : DATATYPE_BASE
{
public:
	void createFromStreamEx(MemoryStream& stream, TArray<uint8>& datas);
	void addToStreamEx(Bundle& stream, const TArray<uint8>& v);
};


